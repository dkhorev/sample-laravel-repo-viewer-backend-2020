# Backend | GitHub repos viewer test

[Task description](requirements.md)

[Execution plan](plan.md)

#### Setup

`composer i`

`cp .env.exmaple .env`

add your MySQL info (db - create it first, add user and pwd) \
APP_KEY already generated for simplicity

`php artisan serve`

add IP of running backend to front end project's .env

#### Tests

`composer test`

![Tests](resources/docs/backend-test.png "Tests")

#### CLI commands

`php artisan repo:import` Import author/repo

`php artisan repo:show` Show commits for specific author/repo

## Упрощения в реализации

+ Данные выкачиваются синхронно, но сделано все на событиях, поэтому на реальном проекте асинхронный метод был бы предпочтительный (переключается легко). Не разрабатывал защиту от зависаний при синхронной работе, т.к. на очень больших репо может произойти таймаут.

+ В модели не включена связь с источником, хотя структура приложения дает возможность лего добавить другие источники: gitlab, bitbucket etc.

+ Для простоты, в Repositories включены вместе методы фильтрации и операций над моделями.
В реальном приложении возможны варианты как объединения, так и разделения или делегирования фильтрации на сервисы отдельные.

+ В реальных проектах на модели предпочитаю делать контракты, если есть возможность в будущем их переезда в другое хранилище. Для MVP/тестового задания не стал делать.

+ Для упрощения не используется паттерн Data Transfer Object. Все данные педераются в ассоциативных массивах. По желанию CTO/заказчиков и исходя из масштаба проекта я бы рассмотрел применение DTO.


# TODO:
+ Add unique constraint to Commit's `sha` + `project_id`, tests
+ Add uniqie constraint to Projects, Author `name`, tests
+ ProjectDownload should have commits_count param with total
+ No `on queue failing` event or error handling
+ Not implementing formatters/view pattern for CLI commands
+ Not making test for CLI commands
+ Only public GitHub repos can be retreived
+ No texts for http errors (only stubs)
+ DeleteCommitsService doesn't delete Project if empty, doesn't delete Author if no Projects left for him
