<?php

namespace Tests\Feature;

use App\Contracts\Services\Project\ProjectAddServiceContract;
use App\Models\Commit;
use App\Models\Project;
use App\Services\Source\GithubFetchService;
use Exception;
use Illuminate\Http\Response;
use Mockery\MockInterface;
use Tests\TestCase;

class RepositoryControllerTest extends TestCase
{
    /** @test */
    public function testIndex()
    {
        // setup
        factory(Commit::class, 3)->create();

        // act
        $response = $this->json('get', route('repo.index'));

        // assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data'  => [
                ['id', 'name', 'author', 'commits_count'],
            ],
            'links' => ['first', 'last', 'prev', 'next'],
            'meta'  => ['current_page', 'per_page', 'total'],
        ]);
    }

    /** @test */
    public function testIndexWithDeleted()
    {
        // setup
        factory(Commit::class, 3)->create();
        Project::all()->each(function (Project $project) {
            $project->delete();
        });

        // act
        $response = $this->json('get', route('repo.index'));

        // assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data'  => [],
            'links' => ['first', 'last', 'prev', 'next'],
            'meta'  => ['current_page', 'per_page', 'total'],
        ]);

        $data = $response->decodeResponseJson()['data'];
        $this->assertCount(0, $data);
    }

    /** @test */
    public function testCommits()
    {
        // setup
        /** @var Project $project */
        $project = factory(Project::class)->create();
        factory(Commit::class, 5)->create(['project_id' => $project]);

        // act
        $response = $this->json('get', route('repo.project.commits', ['project' => $project->id]));

        // assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data'  => [
                ['id', 'sha'],
            ],
            'links' => ['first', 'last', 'prev', 'next'],
            'meta'  => ['current_page', 'per_page', 'total'],
        ]);
    }

    /** @test */
    public function testCommitsAreFilteredByProject()
    {
        // setup
        /** @var Project $project */
        $project = factory(Project::class)->create();
        factory(Commit::class, 5)->create(['project_id' => $project]);
        factory(Commit::class, 10)->create();

        // act
        $response = $this->json('get', route('repo.project.commits', ['project' => $project->id]));

        // assert
        $response->assertStatus(Response::HTTP_OK);
        $data = $response->decodeResponseJson()['data'];
        $this->assertCount(5, $data);
        $expected = Commit::where('project_id', $project->id)->get()->pluck('id');
        $this->assertEquals(collect($data)->pluck('id'), $expected);
    }

    /** @test */
    public function test404Project()
    {
        // act
        $response = $this->json('get', route('repo.project.commits', ['project' => 9999]));

        // assert
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function invalidAliasProvider()
    {
        return [
            ['dkhorev'],
            ['dkhorev/'],
            ['/123'],
            ['/'],
            [''],
            [null],
            [false],
        ];
    }

    /**
     * @test
     * @dataProvider invalidAliasProvider
     *
     * @param $alias
     */
    public function testImportWithInvalidAlias($alias)
    {
        // act
        $response = $this->json('post', route('repo.import'), [
            'alias' => $alias,
        ]);

        // assert
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function testImportCorrectAliasHappyPath()
    {
        // setup | pre assert
        $this->mock(ProjectAddServiceContract::class, function (MockInterface $mock) {
            $mock->shouldReceive('add')
                 ->once()
                 ->with(GithubFetchService::class, 'dkhorev', 'test')
                 ->andReturn(true);
        });

        // act
        $response = $this->json('post', route('repo.import'), [
            'alias' => 'dkhorev/test',
        ]);

        // assert
        $response->assertStatus(Response::HTTP_CREATED);
    }

    /** @test */
    public function testImportWithGuzzleException()
    {
        // setup | pre assert
        $this->mock(ProjectAddServiceContract::class, function (MockInterface $mock) {
            $mock->shouldReceive('add')
                 ->once()
                 ->with(GithubFetchService::class, 'dkhorev', 'test')
                 ->andThrow(Exception::class);
        });

        // act
        $response = $this->json('post', route('repo.import'), [
            'alias' => 'dkhorev/test',
        ]);

        // assert
        $response->assertStatus(Response::HTTP_CONFLICT);
    }

    /** @test */
    public function testImportWhen404OrPrivate()
    {
        // setup | pre assert
        $this->mock(ProjectAddServiceContract::class, function (MockInterface $mock) {
            $mock->shouldReceive('add')
                 ->once()
                 ->with(GithubFetchService::class, 'dkhorev', 'test')
                 ->andReturn(false);
        });

        // act
        $response = $this->json('post', route('repo.import'), [
            'alias' => 'dkhorev/test',
        ]);

        // assert
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function testDelete404Project()
    {
        // act
        $response = $this->json('get', route('repo.delete', ['project' => 99999]));

        // assert
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function testDeletionOfCommits()
    {
        // setup
        /** @var Project $project */
        $project = factory(Project::class)->create();
        $commit1 = factory(Commit::class)->create(['project_id' => $project]);
        $commit2 = factory(Commit::class)->create(['project_id' => $project]);

        $commitOther = factory(Commit::class)->create();

        // act
        $response = $this->json('delete', route('repo.delete', ['project' => $project->id]), [
            'commits' => [
                $commit1->id,
                $commit2->id,
                $commitOther->id,
            ],
        ]);

        // assert
        $response->assertStatus(Response::HTTP_OK);
        // assert
        $this->assertTrue($commit1->fresh()->trashed());
        $this->assertTrue($commit2->fresh()->trashed());
        $this->assertFalse($commitOther->fresh()->trashed());
    }

    public function invalidDeleteProvider()
    {
        return [
            [
                ['commits' => []]
            ],
            [[false]],
            [[null]],
            [[false]],
        ];
    }

    /**
     * @test
     * @dataProvider invalidDeleteProvider
     *
     * @param $data
     */
    public function testDeleteInvalidRequests($data)
    {
        // setup
        /** @var Project $project */
        $project = factory(Project::class)->create();

        // act
        $response = $this->json('delete', route('repo.delete', ['project' => $project->id]), $data);

        // assert
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
