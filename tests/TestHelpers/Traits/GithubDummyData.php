<?php

namespace Tests\TestHelpers\Traits;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\Str;

/**
 * Trait GithubDummyData
 *
 * @package Tests\TestHelpers\Traits
 */
trait GithubDummyData
{
    /**
     * @var Generator
     */
    private $faker;

    /**
     * status 200
     *
     * @param int $times
     *
     * @return array
     */
    public function gitHubDataResponse($times = 1)
    {
        // Header: Link => <https://api.github.com/repositories/190873208/commits?per_page=10&page=2>; rel="next", <https://api.github.com/repositories/190873208/commits?per_page=10&page=2>; rel="last"

        if (!$this->faker) {
            $this->faker = Factory::create();
        }

        $data = [];

        while ($times-- >= 1) {
            $data[] = $this->randomCommit();
        }

        return $data;
    }

    protected function randomCommit()
    {
        return [
            'sha'    => Str::random(40),
            'commit' => [
                'message' => $this->faker->text(30),
            ],
        ];
    }

    /**
     * Empty array and/or no Link header with `rel="next"`
     * status 200
     * @return array
     */
    public function gitHubLastPageResponse()
    {
        // Header: Link => <https://api.github.com/repositories/190873208/commits?per_page=10&page=2>; rel="prev", <https://api.github.com/repositories/190873208/commits?per_page=10&page=2>; rel="first"

        return [];
    }

    /**
     * status 404
     *
     * @return array
     */
    public function gitHubNotFoundOrPrivateResponse()
    {
        return [
            "message" => "Not Found",
        ];
    }

    /**
     * status 409
     *
     * @return array
     */
    public function gitHubEmptyRepoResponse()
    {
        return [
            "message" => "Git Repository is empty.",
        ];
    }
}
