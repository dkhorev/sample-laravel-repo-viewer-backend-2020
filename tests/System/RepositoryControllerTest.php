<?php

namespace Tests\System;

use App\Models\Author;
use App\Models\Commit;
use App\Models\Project;
use App\Models\ProjectDownload;
use Illuminate\Http\Response;
use Tests\TestCase;

/**
 * Class RepositoryControllerTest
 * Real request to github api
 *
 * @package Tests\System
 */
class RepositoryControllerTest extends TestCase
{
    /** @test */
    public function testRealDataRequest()
    {
        // act
        $this->json('post', route('repo.import'), [
            'alias' => 'bahdcasts/testing-javascript-2019',
        ]);

        // assert
        $this->assertEquals('bahdcasts', Author::first()->name);
        $this->assertEquals('testing-javascript-2019', Project::first()->name);
    }

    /** @test */
    public function testRealDataException()
    {
        // act
        $response = $this->json('post', route('repo.import'), [
            'alias' => 'dkhorev/test',
        ]);

        // assert
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertCount(0, Author::all());
        $this->assertCount(0, Project::all());
        $this->assertCount(0, Commit::all());
        $this->assertCount(0, ProjectDownload::all());
    }
}
