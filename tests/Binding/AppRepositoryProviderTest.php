<?php

namespace Tests\Binding;

use App\Contracts\Repositories\AuthorRepoContract;
use App\Contracts\Repositories\CommitRepoContract;
use App\Contracts\Repositories\ProjectDownloadRepoContract;
use App\Contracts\Repositories\ProjectRepoContract;
use App\Repositories\AuthorRepository;
use App\Repositories\CommitRepository;
use App\Repositories\ProjectDownloadRepository;
use App\Repositories\ProjectRepository;
use Tests\TestCase;

class AppRepositoryProviderTest extends TestCase
{
    /**
     * @return array
     */
    public function repoProvider()
    {
        return [
            'Author repo binding'          => [AuthorRepoContract::class, AuthorRepository::class],
            'Project repo binding'         => [ProjectRepoContract::class, ProjectRepository::class],
            'Commit repo binding'          => [CommitRepoContract::class, CommitRepository::class],
            'ProjectDownloadEvent repo binding' => [ProjectDownloadRepoContract::class, ProjectDownloadRepository::class],
        ];
    }

    /**
     * @test
     * @dataProvider repoProvider
     *
     * @param string $contract
     * @param string $class
     */
    public function testRepoBindings(string $contract, string $class)
    {
        // act
        $repo = app($contract);

        // assert
        $this->assertInstanceOf($contract, $repo);
        $this->assertInstanceOf($class, $repo);
    }
}
