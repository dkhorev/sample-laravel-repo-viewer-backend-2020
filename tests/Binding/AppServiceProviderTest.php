<?php

namespace Tests\Binding;

use App\Contracts\Services\Commit\DeleteCommitsServiceContract;
use App\Contracts\Services\Project\ListCommitsServiceContract;
use App\Contracts\Services\Project\ListProjectsServiceContract;
use App\Contracts\Services\Project\ProjectAddServiceContract;
use App\Contracts\Services\Source\ImportServiceContract;
use App\Services\Commit\DeleteCommitsService;
use App\Services\Project\ListCommitsService;
use App\Services\Project\ListProjectsService;
use App\Services\Project\ProjectAddService;
use App\Services\Source\ImportService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Tests\TestCase;

class AppServiceProviderTest extends TestCase
{
    /**
     * @return array
     */
    public function serviceProvider()
    {
        return [
            'ClientInterface binding'     => [ClientInterface::class, Client::class],
            'ImportService binding'       => [ImportServiceContract::class, ImportService::class],
            'ProjectAddService binding'   => [ProjectAddServiceContract::class, ProjectAddService::class],
            'ListProjectsService binding' => [ListProjectsServiceContract::class, ListProjectsService::class],
            'ListCommitsService binding' => [ListCommitsServiceContract::class, ListCommitsService::class],
            'DeleteCommitsService binding' => [DeleteCommitsServiceContract::class, DeleteCommitsService::class],
        ];
    }

    /**
     * @test
     * @dataProvider serviceProvider
     *
     * @param string $contract
     * @param string $class
     */
    public function testServiceBindings(string $contract, string $class)
    {
        // act
        $repo = app($contract);

        // assert
        $this->assertInstanceOf($contract, $repo);
        $this->assertInstanceOf($class, $repo);
    }
}
