<?php

namespace Tests\Unit\Services\Commit;

use App\Contracts\Services\Commit\DeleteCommitsServiceContract;
use App\Models\Commit;
use App\Models\Project;
use App\Services\Commit\DeleteCommitsService;
use Tests\TestCase;

class DeleteCommitsServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var DeleteCommitsServiceContract $service */
        $service = app(DeleteCommitsService::class);

        $this->assertInstanceOf(DeleteCommitsService::class, $service);
        $this->assertInstanceOf(DeleteCommitsServiceContract::class, $service);
    }

    /** @test */
    public function testDeletesCommit()
    {
        // setup
        /** @var Commit $commit1 */
        $commit1 = factory(Commit::class)->create();
        /** @var Commit $commit2 */
        $commit2 = factory(Commit::class)->create();

        // act
        /** @var DeleteCommitsServiceContract $service */
        $service = app(DeleteCommitsService::class);
        $service->deleteMany($commit1->project_id, [$commit1->id]);

        // assert
        $this->assertTrue($commit1->fresh()->trashed());
        $this->assertFalse($commit2->fresh()->trashed());
    }

    /** @test */
    public function testDeleteManyAndOnlyOneProject()
    {
        // setup
        /** @var Project $project */
        $project = factory(Project::class)->create();
        $commit1 = factory(Commit::class)->create(['project_id' => $project]);
        $commit2 = factory(Commit::class)->create(['project_id' => $project]);

        $commitOther = factory(Commit::class)->create();

        // act
        /** @var DeleteCommitsServiceContract $service */
        $service = app(DeleteCommitsService::class);
        $service->deleteMany($commit1->project_id, [$commit1->id, $commit2->id, $commitOther->id]);

        // assert
        $this->assertTrue($commit1->fresh()->trashed());
        $this->assertTrue($commit2->fresh()->trashed());
        $this->assertFalse($commitOther->fresh()->trashed());
    }
}
