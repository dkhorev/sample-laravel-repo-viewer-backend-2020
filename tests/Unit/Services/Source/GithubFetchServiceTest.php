<?php

namespace Tests\Unit\Services\Source;

use App\Constants\ProjectDownloadsStatus;
use App\Contracts\Services\Source\FetchServiceContract;
use App\Events\Services\Source\ProjectDownloadStepFinishedEvent;
use App\Models\Author;
use App\Models\Commit;
use App\Models\Project;
use App\Models\ProjectDownload;
use App\Services\Source\GithubFetchService;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use Mockery\MockInterface;
use Tests\TestCase;
use Tests\TestHelpers\Traits\GithubDummyData;
use Throwable;

class GithubFetchServiceTest extends TestCase
{
    use GithubDummyData;

    /**
     * rel="next" - is correct tag
     */
    const GITHUB_LINK_HEADER_WITH_NEXT    = 'rel="something"; rel="next"; rel="last" rel=next rel=\'next\'';
    const GITHUB_LINK_HEADER_WITHOUT_NEXT = 'rel="something"; rel="last" rel=next rel=\'next\'';

    protected function setUp(): void
    {
        parent::setUp();

        Event::fake();

        config()->set('app.fetch_service.url_template.github', 'https://url/:author/:project');
        config()->set('app.fetch_service.http_client_options', [
            'someparam' => 'somevalue',
        ]);
    }

    /** @test */
    public function testInstantiate()
    {
        $service = app(GithubFetchService::class);

        $this->assertInstanceOf(GithubFetchService::class, $service);
        $this->assertInstanceOf(FetchServiceContract::class, $service);
    }

    /**
     * Initial Request with commits, no next header
     * Expect: job is finished, commits created, project created
     *
     * @test
     * @throws GuzzleException
     * @throws Throwable
     */
    public function testInitialRequestWithCommits()
    {
        // setup
        config()->set('app.fetch_service.per_page', 10);

        // mock | pre assert
        $this->mock(ClientInterface::class, function (MockInterface $mock) {
            $options = array_merge(['someparam' => 'somevalue'], [
                'query' => [
                    'per_page' => 10,
                    'page'     => 1,
                ],
            ]);

            $mock->shouldReceive('request')
                 ->once()
                 ->with(
                     'get',
                     'https://url/author1/project1',
                     $options
                 )
                 ->andReturn(new Response(200, [], json_encode($this->gitHubDataResponse(5))));
        });

        // act
        /** @var GithubFetchService $service */
        $service = app(GithubFetchService::class);
        $result = $service->initProjectDownload('author1', 'project1');

        // assert
        $job = ProjectDownload::first();
        $this->assertTrue($result);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(5, Commit::all());
        $this->assertCount(5, $job->project->commits);
        $this->assertEquals(ProjectDownloadsStatus::STATUS_FINISHED, $job->status);
        $this->assertEquals(1, $job->pages_parsed);
        $this->assertEquals('author1', $job->project->author->name);
        $this->assertEquals('project1', $job->project->name);
        Event::assertDispatched(ProjectDownloadStepFinishedEvent::class);
    }

    /**
     * Initial Request, empty commits
     * Expect: job finished, no commits, project created
     *
     * @test
     * @throws GuzzleException
     * @throws Throwable
     */
    public function testInitialRequestNoCommits()
    {
        // setup
        config()->set('app.fetch_service.per_page', 10);

        // mock | pre assert
        $this->mock(ClientInterface::class, function (MockInterface $mock) {
            $options = array_merge(['someparam' => 'somevalue'], [
                'query' => [
                    'per_page' => 10,
                    'page'     => 1,
                ],
            ]);

            $mock->shouldReceive('request')
                 ->once()
                 ->with(
                     'get',
                     'https://url/author1/project1',
                     $options
                 )
                 ->andReturn(new Response(409, [], json_encode($this->gitHubEmptyRepoResponse())));
        });

        // act
        /** @var GithubFetchService $service */
        $service = app(GithubFetchService::class);
        $result = $service->initProjectDownload('author1', 'project1');

        // assert
        $job = ProjectDownload::first();
        $this->assertTrue($result);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(0, Commit::all());
        $this->assertCount(0, $job->project->commits);
        $this->assertEquals(ProjectDownloadsStatus::STATUS_FINISHED, $job->status);
        $this->assertEquals(1, $job->pages_parsed);
        $this->assertEquals('author1', $job->project->author->name);
        $this->assertEquals('project1', $job->project->name);
        Event::assertDispatched(ProjectDownloadStepFinishedEvent::class);
    }

    /**
     * Initial Request, not found
     * Expect: Expect: no job, no project, no commits
     *
     * @test
     * @throws GuzzleException
     * @throws Throwable
     */
    public function testInitialRequestNotFound()
    {
        // setup
        config()->set('app.fetch_service.per_page', 10);

        // mock | pre assert
        $this->mock(ClientInterface::class, function (MockInterface $mock) {
            $options = array_merge(['someparam' => 'somevalue'], [
                'query' => [
                    'per_page' => 10,
                    'page'     => 1,
                ],
            ]);

            $mock->shouldReceive('request')
                 ->once()
                 ->with(
                     'get',
                     'https://url/author1/project1',
                     $options
                 )
                 ->andReturn(new Response(404, [], json_encode($this->gitHubNotFoundOrPrivateResponse())));
        });

        // act
        /** @var GithubFetchService $service */
        $service = app(GithubFetchService::class);
        $result = $service->initProjectDownload('author1', 'project1');

        // assert
        $this->assertFalse($result);
        $this->assertCount(0, Author::all());
        $this->assertCount(0, Project::all());
        $this->assertCount(0, ProjectDownload::all());
        $this->assertCount(0, Commit::all());
        Event::assertNotDispatched(ProjectDownloadStepFinishedEvent::class);
    }

    /**
     * Initial Request, with commits, with next header
     * Expect: Expect: job in progress, commits created, project created
     *
     * @test
     * @throws GuzzleException
     * @throws Throwable
     */
    public function testInitialRequestWithCommitsWithNextPage()
    {
        // setup
        config()->set('app.fetch_service.per_page', 5);

        // mock | pre assert
        $this->mock(ClientInterface::class, function (MockInterface $mock) {
            $options = array_merge(['someparam' => 'somevalue'], [
                'query' => [
                    'per_page' => 5,
                    'page'     => 1,
                ],
            ]);

            $mock->shouldReceive('request')
                 ->once()
                 ->with(
                     'get',
                     'https://url/author1/project1',
                     $options
                 )
                 ->andReturn(new Response(
                     200,
                     ['Link' => static::GITHUB_LINK_HEADER_WITH_NEXT],
                     json_encode($this->gitHubDataResponse(5))
                 ));
        });

        // act
        /** @var GithubFetchService $service */
        $service = app(GithubFetchService::class);
        $result = $service->initProjectDownload('author1', 'project1');

        // assert
        $job = ProjectDownload::first();
        $this->assertTrue($result);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(5, Commit::all());
        $this->assertCount(5, $job->project->commits);
        $this->assertEquals(ProjectDownloadsStatus::STATUS_PROGRESS, $job->status);
        $this->assertEquals(1, $job->pages_parsed);
        $this->assertEquals('author1', $job->project->author->name);
        $this->assertEquals('project1', $job->project->name);
        Event::assertDispatched(ProjectDownloadStepFinishedEvent::class);
    }

    /**
     * Next page request, no commmits, no next page
     * Expect: Expect: job finished, no new commits, pages_parsed + 1
     *
     * @test
     * @throws GuzzleException
     * @throws Throwable
     */
    public function testNextPageNoCommitsNoNextPage()
    {
        // setup
        config()->set('app.fetch_service.per_page', 3);

        // setup job
        /** @var ProjectDownload $job */
        $job = factory(ProjectDownload::class)->create([
            'status'       => ProjectDownloadsStatus::STATUS_PROGRESS,
            'pages_parsed' => 1,
        ]);
        $job->project->commits()->create(['sha' => Str::random(40)]);
        $job->project->commits()->create(['sha' => Str::random(40)]);
        $job->project->commits()->create(['sha' => Str::random(40)]);

        // mock | pre assert
        $this->mock(ClientInterface::class, function (MockInterface $mock) use ($job) {
            $options = array_merge(['someparam' => 'somevalue'], [
                'query' => [
                    'per_page' => 3,
                    'page'     => 2,
                ],
            ]);

            $mock->shouldReceive('request')
                 ->once()
                 ->with(
                     'get',
                     "https://url/{$job->project->author->name}/{$job->project->name}",
                     $options
                 )
                 ->andReturn(new Response(
                     200,
                     ['Link' => static::GITHUB_LINK_HEADER_WITHOUT_NEXT],
                     json_encode($this->gitHubEmptyRepoResponse())
                 ));
        });

        // act
        /** @var GithubFetchService $service */
        $service = app(GithubFetchService::class);
        $result = $service->nextPage($job);

        // assert
        $job = ProjectDownload::first();
        $this->assertTrue($result);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(3, Commit::all());
        $this->assertCount(3, $job->project->commits);
        $this->assertEquals(ProjectDownloadsStatus::STATUS_FINISHED, $job->status);
        $this->assertEquals(2, $job->pages_parsed);
        Event::assertDispatched(ProjectDownloadStepFinishedEvent::class);
    }

    /**
     * Next page request, has commits, no next page
     * Expect: job finished, has new commits, pages_parsed + 1
     *
     * @test
     * @throws GuzzleException
     * @throws Throwable
     */
    public function testNextPageHasCommitNoNextPage()
    {
        // setup
        config()->set('app.fetch_service.per_page', 3);

        // setup job
        /** @var ProjectDownload $job */
        $job = factory(ProjectDownload::class)->create([
            'status'       => ProjectDownloadsStatus::STATUS_PROGRESS,
            'pages_parsed' => 1,
        ]);
        $job->project->commits()->create(['sha' => Str::random(40)]);
        $job->project->commits()->create(['sha' => Str::random(40)]);
        $job->project->commits()->create(['sha' => Str::random(40)]);

        // mock | pre assert
        $this->mock(ClientInterface::class, function (MockInterface $mock) use ($job) {
            $options = array_merge(['someparam' => 'somevalue'], [
                'query' => [
                    'per_page' => 3,
                    'page'     => 2,
                ],
            ]);

            $mock->shouldReceive('request')
                 ->once()
                 ->with(
                     'get',
                     "https://url/{$job->project->author->name}/{$job->project->name}",
                     $options
                 )
                 ->andReturn(new Response(
                     200,
                     ['Link' => static::GITHUB_LINK_HEADER_WITHOUT_NEXT],
                     json_encode($this->gitHubDataResponse(2))
                 ));
        });

        // act
        /** @var GithubFetchService $service */
        $service = app(GithubFetchService::class);
        $result = $service->nextPage($job);

        // assert
        $job = ProjectDownload::first();
        $this->assertTrue($result);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(5, Commit::all());
        $this->assertCount(5, $job->project->commits);
        $this->assertEquals(ProjectDownloadsStatus::STATUS_FINISHED, $job->status);
        $this->assertEquals(2, $job->pages_parsed);
        Event::assertDispatched(ProjectDownloadStepFinishedEvent::class);
    }

    /**
     * Next page request, has commits, has next page
     * Expect: job in progress, has new commits, pages_parsed + 1
     *
     * @test
     * @throws GuzzleException
     * @throws Throwable
     */
    public function testNextPageHasCommitsHasNextPage()
    {
        // setup
        config()->set('app.fetch_service.per_page', 3);

        // setup job
        /** @var ProjectDownload $job */
        $job = factory(ProjectDownload::class)->create([
            'status'       => ProjectDownloadsStatus::STATUS_PROGRESS,
            'pages_parsed' => 1,
        ]);
        $job->project->commits()->create(['sha' => Str::random(40)]);
        $job->project->commits()->create(['sha' => Str::random(40)]);
        $job->project->commits()->create(['sha' => Str::random(40)]);

        // mock | pre assert
        $this->mock(ClientInterface::class, function (MockInterface $mock) use ($job) {
            $options = array_merge(['someparam' => 'somevalue'], [
                'query' => [
                    'per_page' => 3,
                    'page'     => 2,
                ],
            ]);

            $mock->shouldReceive('request')
                 ->once()
                 ->with(
                     'get',
                     "https://url/{$job->project->author->name}/{$job->project->name}",
                     $options
                 )
                 ->andReturn(new Response(
                     200,
                     ['Link' => static::GITHUB_LINK_HEADER_WITH_NEXT],
                     json_encode($this->gitHubDataResponse(3))
                 ));
        });

        // act
        /** @var GithubFetchService $service */
        $service = app(GithubFetchService::class);
        $result = $service->nextPage($job);

        // assert
        $job = ProjectDownload::first();
        $this->assertTrue($result);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(6, Commit::all());
        $this->assertCount(6, $job->project->commits);
        $this->assertEquals(ProjectDownloadsStatus::STATUS_PROGRESS, $job->status);
        $this->assertEquals(2, $job->pages_parsed);
        Event::assertDispatched(ProjectDownloadStepFinishedEvent::class);
    }
}
