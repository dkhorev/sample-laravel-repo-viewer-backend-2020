<?php

namespace Tests\Unit\Services\Source;

use App\Constants\ProjectDownloadsStatus;
use App\Contracts\Services\Source\ImportServiceContract;
use App\Exceptions\Services\Source\CommitsImportFailedException;
use App\Exceptions\Services\Source\NextPageStatusNotSetException;
use App\Models\Author;
use App\Models\Commit;
use App\Models\Project;
use App\Models\ProjectDownload;
use App\Services\Source\ImportService;
use Exception;
use Tests\TestCase;
use Tests\TestHelpers\Traits\GithubDummyData;
use Throwable;

class ImportServiceTest extends TestCase
{
    use GithubDummyData;

    /** @test */
    public function testInstantiate()
    {
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);

        $this->assertInstanceOf(ImportService::class, $service);
        $this->assertInstanceOf(ImportServiceContract::class, $service);
    }

    /**
     * @test
     * @throws CommitsImportFailedException
     * @throws NextPageStatusNotSetException
     * @throws Throwable
     */
    public function testExceptionWhenNextPageStatusNotSetOnNew()
    {
        // assert
        $this->expectException(NextPageStatusNotSetException::class);

        // act
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);
        $service->importNew('author1', 'project1', []);
    }

    /**
     * @test
     * @throws CommitsImportFailedException
     * @throws NextPageStatusNotSetException
     * @throws Throwable
     */
    public function testExceptionWhenNextPageStatusNotSetOnNext()
    {
        // assert
        $this->expectException(NextPageStatusNotSetException::class);

        // act
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);
        $service->importForJob(factory(ProjectDownload::class)->create(), []);
    }

    public function hasNextPageProvider()
    {
        return [
            'has next page' => [true],
            'no next page'  => [false],
        ];
    }

    /**
     * @test
     * @dataProvider hasNextPageProvider
     *
     * @param bool $hasNextPage
     *
     * @throws CommitsImportFailedException
     * @throws NextPageStatusNotSetException
     * @throws Throwable
     */
    public function testImportNew(bool $hasNextPage)
    {
        // act
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);
        $result = $service->setHasNextPage($hasNextPage)
                          ->importNew('author1', 'project1', $this->gitHubDataResponse(3));

        // assert
        $job = ProjectDownload::first();
        $this->assertEquals($job->id, $result->id);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(3, Commit::all());
        $this->assertCount(3, $result->project->commits);
        $this->assertEquals(
            $hasNextPage ? ProjectDownloadsStatus::STATUS_PROGRESS : ProjectDownloadsStatus::STATUS_FINISHED,
            $result->status
        );
        $this->assertEquals(1, $result->pages_parsed);
        $this->assertEquals('author1', $result->project->author->name);
        $this->assertEquals('project1', $result->project->name);
    }

    /**
     * @test
     * @dataProvider hasNextPageProvider
     *
     * @param bool $hasNextPage
     *
     * @throws NextPageStatusNotSetException
     * @throws CommitsImportFailedException
     * @throws Throwable
     */
    public function testImportForJob(bool $hasNextPage)
    {
        // setup
        /** @var ProjectDownload $job */
        $job = factory(ProjectDownload::class)->create(['pages_parsed' => 3]);

        // pre assert
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(0, Commit::all());
        $this->assertCount(0, $job->project->commits);

        // act
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);
        $result = $service->setHasNextPage($hasNextPage)
                          ->importForJob($job, $this->gitHubDataResponse(4));

        // assert
        $job = $job->fresh();
        $this->assertEquals($job->id, $result->id);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(4, Commit::all());
        $this->assertEquals(
            $hasNextPage ? ProjectDownloadsStatus::STATUS_PROGRESS : ProjectDownloadsStatus::STATUS_FINISHED,
            $result->status
        );
        $this->assertEquals(4, $result->pages_parsed); // 3 initial + 1
        $this->assertCount(4, $job->project->commits);
    }

    /**
     * @test
     * @throws NextPageStatusNotSetException
     * @throws CommitsImportFailedException
     * @throws Throwable
     */
    public function testImportNewWhenAuthorExists()
    {
        // setup
        factory(Author::class)->create(['name' => 'author1']);

        // act
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);
        $result = $service->setHasNextPage(false)
                          ->importNew('author1', 'project1', $this->gitHubEmptyRepoResponse());

        // assert
        $job = ProjectDownload::first();
        $this->assertEquals($job->id, $result->id);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(1, Project::all());
        $this->assertEquals('author1', $result->project->author->name);
        $this->assertEquals('project1', $result->project->name);
    }

    /**
     * @test
     * @throws Exception
     * @throws Throwable
     */
    public function testImportNewWhenAuthorDeleted()
    {
        // setup
        factory(Author::class)->create(['name' => 'author1'])->delete();

        // pre assert
        $this->assertCount(0, Author::all());

        // act
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);
        $result = $service->setHasNextPage(false)
                          ->importNew('author1', 'project1', $this->gitHubEmptyRepoResponse());

        // assert
        $job = ProjectDownload::first();
        $this->assertEquals($job->id, $result->id);
        $this->assertCount(1, Author::withTrashed()->get());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(1, Project::all());
        $this->assertEquals('author1', $result->project->author->name);
        $this->assertEquals('project1', $result->project->name);
    }

    /**
     * @test
     * @throws Exception
     * @throws Throwable
     */
    public function testImportNewWhenProjectExists()
    {
        // setup
        $author = factory(Author::class)->create(['name' => 'author1']);
        factory(Project::class)->create(['name' => 'project1', 'author_id' => $author->id]);

        // act
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);
        $result = $service->setHasNextPage(false)
                          ->importNew('author1', 'project1', $this->gitHubEmptyRepoResponse());

        // assert
        $job = ProjectDownload::first();
        $this->assertEquals($job->id, $result->id);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(1, Project::all());
        $this->assertEquals('author1', $result->project->author->name);
        $this->assertEquals('project1', $result->project->name);
    }

    /**
     * @test
     * @throws Exception
     * @throws Throwable
     */
    public function testImportNewWhenProjectDeleted()
    {
        // setup
        $author = factory(Author::class)->create(['name' => 'author1']);
        $project = factory(Project::class)->create(['name' => 'project1', 'author_id' => $author->id]);
        $project->delete();

        // pre assert
        $this->assertCount(0, Project::all());

        // act
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);
        $result = $service->setHasNextPage(false)
                          ->importNew('author1', 'project1', $this->gitHubEmptyRepoResponse());

        // assert
        $job = ProjectDownload::first();
        $this->assertEquals($job->id, $result->id);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, ProjectDownload::all());
        $this->assertCount(1, Project::withTrashed()->get());
        $this->assertEquals('author1', $result->project->author->name);
        $this->assertEquals('project1', $result->project->name);
    }

    /**
     * @test
     * @throws Exception
     * @throws Throwable
     */
    public function testImportNewWhenCommitExists()
    {
        // setup
        /** @var Commit $commit */
        $commit = factory(Commit::class)->create();

        // act
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);
        $result = $service->setHasNextPage(false)
                          ->importNew($commit->project->author->name, $commit->project->name, [$commit->toArray()]);

        // assert
        $job = ProjectDownload::first();
        $this->assertEquals($job->id, $result->id);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(1, Commit::all());
    }

    /**
     * @test
     * @throws Exception
     * @throws Throwable
     */
    public function testImportNewWhenHasCommitsDeleted()
    {
        // setup
        $commits = $this->gitHubDataResponse(3);
        /** @var Project $project */
        $project = factory(Project::class)->create();

        foreach ($commits as $commit) {
            factory(Commit::class)->create([
                'sha'        => $commit['sha'],
                'project_id' => $project->id,
            ])->delete();
        }

        // pre assert
        $this->assertCount(0, Commit::all());

        // act
        /** @var ImportServiceContract $service */
        $service = app(ImportService::class);
        $result = $service->setHasNextPage(false)
                          ->importNew($project->author->name, $project->name, $commits);

        // assert
        $job = ProjectDownload::first();
        $this->assertEquals($job->id, $result->id);
        $this->assertCount(1, Author::all());
        $this->assertCount(1, Project::all());
        $this->assertCount(3, Commit::withTrashed()->get());
    }
}
