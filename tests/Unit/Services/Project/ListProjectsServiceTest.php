<?php

namespace Tests\Unit\Services\Project;

use App\Contracts\Services\Project\ListProjectsServiceContract;
use App\Models\Project;
use App\Services\Project\ListProjectsService;
use Tests\TestCase;

class ListProjectsServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var ListProjectsServiceContract $service */
        $service = app(ListProjectsService::class);

        $this->assertInstanceOf(ListProjectsService::class, $service);
        $this->assertInstanceOf(ListProjectsServiceContract::class, $service);
    }

    /** @test */
    public function testGetWithoutDeleted()
    {
        // setup
        factory(Project::class, 3)->create();
        Project::all()->each(function (Project $project) {
            $project->delete();
        });
        $project1 = factory(Project::class)->create();
        $project2 = factory(Project::class)->create();

        // act
        /** @var ListProjectsServiceContract $service */
        $service = app(ListProjectsService::class);
        $result = $service->get();

        // assert
        $this->assertCount(2, $result);
        $this->assertEquals($project1->id, $result->first()->id);
        $this->assertEquals($project2->id, $result->last()->id);
    }
}
