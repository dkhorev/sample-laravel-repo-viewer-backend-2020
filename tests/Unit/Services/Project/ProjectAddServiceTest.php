<?php

namespace Tests\Unit\Services\Project;

use App\Constants\ProjectDownloadsStatus;
use App\Contracts\Services\Project\ProjectAddServiceContract;
use App\Contracts\Services\Source\FetchServiceContract;
use App\Models\Author;
use App\Models\Commit;
use App\Models\Project;
use App\Models\ProjectDownload;
use App\Services\Project\ProjectAddService;
use App\Services\Source\GithubFetchService;
use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Mockery\MockInterface;
use Tests\TestCase;

class ProjectAddServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var ProjectAddServiceContract $service */
        $service = app(ProjectAddService::class);

        $this->assertInstanceOf(ProjectAddService::class, $service);
        $this->assertInstanceOf(ProjectAddServiceContract::class, $service);
    }

    /** @test */
    public function testWithExceptions()
    {
        // mock
        $this->mock(FetchServiceContract::class, function (MockInterface $mock) {
            $mock->shouldReceive('initProjectDownload')
                 ->with('author1', 'project1')
                 ->andThrow(Exception::class);
        });

        // act
        /** @var ProjectAddServiceContract $service */
        $service = app(ProjectAddService::class);
        $result = $service->add(app(FetchServiceContract::class), 'author1', 'project1');

        // assert
        $this->assertFalse($result);
    }

    /**
     * @test
     */
    public function testAddNew()
    {
        // mock
        $this->mock(FetchServiceContract::class, function (MockInterface $mock) {
            $mock->shouldReceive('initProjectDownload')
                 ->with('author1', 'project1')
                 ->andReturn(true);
        });

        // act
        /** @var ProjectAddServiceContract $service */
        $service = app(ProjectAddService::class);
        $result = $service->add(app(FetchServiceContract::class), 'author1', 'project1');

        // assert
        $this->assertTrue($result);
    }

    /** @test */
    public function testEventDispatchedAndHasListener()
    {
        // setup
        config()->set('app.fetch_service.url_template.github', 'https://url/:author/:project');
        config()->set('app.fetch_service.http_client_options', [
            'someparam' => 'somevalue',
        ]);
        config()->set('app.fetch_service.per_page', 5);

        // mock | pre assert
        $this->mock(ClientInterface::class, function (MockInterface $mock) {
            $options = array_merge(['someparam' => 'somevalue'], [
                'query' => [
                    'per_page' => 5,
                    'page'     => 1,
                ],
            ]);

            $mock->shouldReceive('request')
                 ->once()
                 ->with(
                     'get',
                     'https://url/author1/project1',
                     $options
                 )
                 ->andReturn(new Response(200, [], json_encode([])));
        });

        // act
        /** @var ProjectAddServiceContract $service */
        $service = app(ProjectAddService::class);
        $result = $service->add(app(GithubFetchService::class), 'author1', 'project1');

        // assert
        $this->assertTrue($result);
        $this->assertCount(1, Project::all());
        $this->assertCount(1, Author::all());
        $this->assertCount(0, Commit::all());
        $job = ProjectDownload::first();
        $this->assertEquals(ProjectDownloadsStatus::STATUS_FINISHED, $job->status);
        $this->assertEquals('author1', $job->project->author->name);
        $this->assertEquals('project1', $job->project->name);

        // assert for async listeners | add Queue::fake();
        // Queue::assertPushed(CallQueuedListener::class, function ($job) {
        //     return $job->class == ProjectDownloadStepFinishedListener::class;
        // });
    }
}
