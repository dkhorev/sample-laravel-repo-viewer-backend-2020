<?php

namespace Tests\Unit\Services\Project;

use App\Contracts\Services\Project\ListCommitsServiceContract;
use App\Models\Commit;
use App\Models\Project;
use App\Services\Project\ListCommitsService;
use Tests\TestCase;

class ListCommitsServiceTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var ListCommitsServiceContract $service */
        $service = app(ListCommitsService::class);

        $this->assertInstanceOf(ListCommitsService::class, $service);
        $this->assertInstanceOf(ListCommitsServiceContract::class, $service);
    }

    /** @test */
    public function testGetWithoutDeleted()
    {
        // setup
        /** @var Project $project */
        $project = factory(Project::class)->create();
        factory(Commit::class, 3)->create(['project_id' => $project]);
        Commit::all()->each(function (Commit $commit) {
            $commit->delete();
        });
        $commit1 = factory(Commit::class)->create(['project_id' => $project]);
        $commit2 = factory(Commit::class)->create(['project_id' => $project]);

        // act
        /** @var ListCommitsServiceContract $service */
        $service = app(ListCommitsService::class);
        $result = $service->getForProject($project);

        // assert
        $this->assertCount(2, $result);
        $this->assertEquals($commit1->id, $result->first()->id);
        $this->assertEquals($commit2->id, $result->last()->id);
    }
}
