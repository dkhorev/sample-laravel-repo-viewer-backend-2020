<?php

namespace Tests\Unit\Models;

use App\Models\Author;
use App\Models\Project;
use Tests\TestCase;

class AuthorTest extends TestCase
{
    /** @test */
    public function testModelFields()
    {
        /** @var Author $model */
        $model = factory(Author::class)->create();

        $this->assertNotNull($model->id);
        $this->assertNotNull($model->name);
        $this->assertNotNull($model->created_at);
        $this->assertNotNull($model->updated_at);
        $this->assertNull($model->deleted_at);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testHasSoftDeletes()
    {
        // setup
        /** @var Author $model */
        $model = factory(Author::class)->create();

        // act
        $model->delete();

        // assert
        $modelNew = Author::withTrashed()->first();
        $this->assertNotNull($modelNew->deleted_at);
        $this->assertEquals($model->id, $modelNew->id);
    }

    /** @test */
    public function testFillableParams()
    {
        // act
        $date = now()->subDay()->startOfDay();
        $model = Author::create([
            'name'       => 'Bob',
            'created_at' => $date,
            'updated_at' => $date,
        ]);

        // assert
        $this->assertEquals('Bob', $model->name);
        $this->assertEquals($date, $model->created_at);
        $this->assertEquals($date, $model->updated_at);
        $this->assertNull($model->deleted_at);
    }

    /** @test */
    public function testProjectsRelation()
    {
        // setup
        /** @var Author $model */
        $model = factory(Author::class)->create();
        $project1 = factory(Project::class)->create();
        $project2 = factory(Project::class)->create();
        $model->projects()->save($project1);
        $model->projects()->save($project2);

        // assert
        $result = $model->projects;
        $this->assertEquals(2, $result->count());
        $this->assertEquals($project1->id, $result->first()->id);
        $this->assertEquals($project2->id, $result->last()->id);
    }
}
