<?php

namespace Tests\Unit\Models;

use App\Models\Author;
use App\Models\Project;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    /** @test */
    public function testModelFields()
    {
        /** @var Project $model */
        $model = factory(Project::class)->create();

        $this->assertNotNull($model->id);
        $this->assertNotNull($model->name);
        $this->assertNotNull($model->created_at);
        $this->assertNotNull($model->updated_at);
        $this->assertNull($model->deleted_at);
        $this->assertNotNull($model->author_id);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testHasSoftDeletes()
    {
        // setup
        /** @var Project $model */
        $model = factory(Project::class)->create();

        // act
        $model->delete();

        // assert
        $modelNew = Project::withTrashed()->first();
        $this->assertNotNull($modelNew->deleted_at);
        $this->assertEquals($model->id, $modelNew->id);
    }

    /** @test */
    public function testFillableParams()
    {
        // act
        $date = now()->subDay()->startOfDay();
        $model = Project::create([
            'name'       => 'React',
            'author_id'  => factory(Author::class)->create()->id,
            'created_at' => $date,
            'updated_at' => $date,
        ]);

        // assert
        $this->assertEquals('React', $model->name);
        $this->assertEquals($date, $model->created_at);
        $this->assertEquals($date, $model->updated_at);
        $this->assertNull($model->deleted_at);
        $this->assertNotNull($model->author_id);
    }

    /** @test */
    public function testAuthorRelation()
    {
        // setup
        /** @var Project $model */
        $model = factory(Project::class)->create();

        // assert
        $this->assertNotNull($model->author_id);
        $this->assertNotNull($model->author);
    }
}
