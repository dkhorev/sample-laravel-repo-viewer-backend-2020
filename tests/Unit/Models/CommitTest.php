<?php

namespace Tests\Unit\Models;

use App\Models\Commit;
use App\Models\Project;
use Tests\TestCase;

class CommitTest extends TestCase
{
    /** @test */
    public function testModelFields()
    {
        /** @var Commit $model */
        $model = factory(Commit::class)->create();

        $this->assertNotNull($model->id);
        $this->assertNotNull($model->sha);
        $this->assertNotNull($model->created_at);
        $this->assertNotNull($model->updated_at);
        $this->assertNull($model->deleted_at);
        $this->assertNotNull($model->project);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testHasSoftDeletes()
    {
        // setup
        /** @var Commit $model */
        $model = factory(Commit::class)->create();

        // act
        $model->delete();

        // assert
        $modelNew = Commit::withTrashed()->first();
        $this->assertNotNull($modelNew->deleted_at);
        $this->assertEquals($model->id, $modelNew->id);
    }

    /** @test */
    public function testFillableParams()
    {
        // act
        $date = now()->subDay()->startOfDay();
        $model = Commit::create([
            'sha'        => 'qwe123',
            'project_id' => factory(Project::class)->create()->id,
            'created_at' => $date,
            'updated_at' => $date,
        ]);

        // assert
        $this->assertEquals('qwe123', $model->sha);
        $this->assertEquals($date, $model->created_at);
        $this->assertEquals($date, $model->updated_at);
        $this->assertNull($model->deleted_at);
        $this->assertNotNull($model->project_id);
    }

    /** @test */
    public function testProjectRelation()
    {
        // setup
        /** @var Commit $model */
        $model = factory(Commit::class)->create();

        // assert
        $this->assertNotNull($model->project_id);
        $this->assertNotNull($model->project);
    }
}
