<?php

namespace Tests\Unit\Models;

use App\Constants\ProjectDownloadsStatus;
use App\Models\Project;
use App\Models\ProjectDownload;
use Tests\TestCase;

class ProjectDownloadTest extends TestCase
{
    /** @test */
    public function testModelFields()
    {
        /** @var ProjectDownload $model */
        $model = factory(ProjectDownload::class)->create();
        $model = $model->fresh();

        $this->assertNotNull($model->id);
        $this->assertNotNull($model->created_at);
        $this->assertNotNull($model->updated_at);
        $this->assertNotNull($model->project_id);
        $this->assertNotNull($model->pages_parsed);
        $this->assertNotNull($model->status);
    }

    /** @test */
    public function testFillableParams()
    {
        // act
        $date = now()->subDay()->startOfDay();
        $model = ProjectDownload::create([
            'project_id'   => factory(Project::class)->create()->id,
            'pages_parsed' => 10,
            'status'       => ProjectDownloadsStatus::STATUS_FINISHED,
            'created_at'   => $date,
            'updated_at'   => $date,
        ]);

        // assert
        $this->assertEquals($date, $model->created_at);
        $this->assertEquals($date, $model->updated_at);
        $this->assertNotNull($model->project_id);
        $this->assertEquals(10, $model->pages_parsed);
        $this->assertEquals(ProjectDownloadsStatus::STATUS_FINISHED, $model->status);
    }

    /** @test */
    public function testProjectRelation()
    {
        // setup
        /** @var ProjectDownload $model */
        $model = factory(ProjectDownload::class)->create();

        // assert
        $this->assertNotNull($model->project_id);
        $this->assertNotNull($model->project);
    }
}
