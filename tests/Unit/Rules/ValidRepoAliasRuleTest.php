<?php

namespace Tests\Unit\Rules;

use App\Rules\ValidRepoAliasRule;
use Tests\TestCase;

class ValidRepoAliasRuleTest extends TestCase
{
    public function correctAliasProvider()
    {
        return [
            ['dkhorev/test'],
            ['laravel/123'],
        ];
    }

    /**
     * @test
     * @dataProvider correctAliasProvider
     *
     * @param string $repoAlias
     */
    public function testCorrectRepoPasses(string $repoAlias)
    {
        /** @var ValidRepoAliasRule $rule */
        $rule = app(ValidRepoAliasRule::class);

        $this->assertTrue($rule->passes('', $repoAlias));
    }

    public function invalidAliasProvider()
    {
        return [
            ['dkhorev'],
            ['dkhorev/'],
            ['/123'],
            ['/'],
            [''],
            [null],
            [false],
        ];
    }

    /**
     * @test
     * @dataProvider invalidAliasProvider
     *
     * @param mixed $repoAlias
     */
    public function testInvalidRepoPasses($repoAlias)
    {
        /** @var ValidRepoAliasRule $rule */
        $rule = app(ValidRepoAliasRule::class);

        $this->assertFalse($rule->passes('', $repoAlias));
    }


}
