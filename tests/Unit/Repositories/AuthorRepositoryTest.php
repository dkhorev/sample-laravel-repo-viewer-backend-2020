<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Repositories\AuthorRepoContract;
use App\Models\Author;
use App\Repositories\AuthorRepository;
use Tests\TestCase;

class AuthorRepositoryTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        /** @var AuthorRepository $repo */
        $repo = app(AuthorRepository::class);

        $this->assertInstanceOf(AuthorRepository::class, $repo);
        $this->assertInstanceOf(AuthorRepoContract::class, $repo);
    }

    /** @test */
    public function testModelBinding()
    {
        // setup
        factory(Author::class, 3)->create();

        // act
        /** @var AuthorRepository $repo */
        $repo = app(AuthorRepository::class);
        $result = $repo->get();

        // assert
        $this->assertCount(3, $result);
        foreach ($result as $model) {
            $this->assertInstanceOf(Author::class, $model);
        }
    }
}
