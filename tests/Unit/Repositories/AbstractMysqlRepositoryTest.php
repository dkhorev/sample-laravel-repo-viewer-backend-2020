<?php

namespace Tests\Unit\Repositories;

use App\Models\Author;
use App\Repositories\AuthorRepository;
use Exception;
use Tests\TestCase;

class AbstractMysqlRepositoryTest extends TestCase
{
    const TEST_CLASS = Author::class;
    const TEST_REPO  = AuthorRepository::class;

    /**
     * @var AuthorRepository
     */
    protected $repo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repo = app(static::TEST_REPO);
    }

    /** @test */
    public function testCreate()
    {
        // setup
        $model = factory(static::TEST_CLASS)->make()->toArray();

        // act
        /** @var Author $result */
        $result = $this->repo->create($model);

        // assert
        $this->assertEquals($model['name'], $result->name);
    }

    /**
     * @test
     * @throws Exception
     */
    public function testDelete()
    {
        // setup
        $model = factory(static::TEST_CLASS)->create();

        // pre assert
        $this->assertEquals(1, app(static::TEST_CLASS)->count());

        // act
        $this->repo->delete($model);

        // pre assert
        $this->assertEquals(0, app(static::TEST_CLASS)->count());
    }

    /** @test */
    public function testFindById()
    {
        // setup
        $model = factory(static::TEST_CLASS)->create();
        factory(static::TEST_CLASS, 5)->create();

        // act
        /** @var Author $result */
        $result = $this->repo->findById($model->id);

        // assert
        $this->assertInstanceOf(static::TEST_CLASS, $model);
        $this->assertEquals($result->id, $model->id);
    }
}
