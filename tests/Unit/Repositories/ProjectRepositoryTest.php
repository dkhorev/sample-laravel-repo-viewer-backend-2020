<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Repositories\ProjectRepoContract;
use App\Models\Project;
use App\Repositories\ProjectRepository;
use Tests\TestCase;

class ProjectRepositoryTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        $repo = app(ProjectRepository::class);

        $this->assertInstanceOf(ProjectRepository::class, $repo);
        $this->assertInstanceOf(ProjectRepoContract::class, $repo);
    }

    /** @test */
    public function testModelBinding()
    {
        // setup
        factory(Project::class, 3)->create();

        // act
        /** @var ProjectRepository $repo */
        $repo = app(ProjectRepository::class);
        $result = $repo->get();

        // assert
        $this->assertCount(3, $result);
        foreach ($result as $model) {
            $this->assertInstanceOf(Project::class, $model);
        }
    }
}
