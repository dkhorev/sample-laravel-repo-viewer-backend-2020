<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Repositories\CommitRepoContract;
use App\Models\Commit;
use App\Repositories\CommitRepository;
use Tests\TestCase;

class CommitRepositoryTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        $repo = app(CommitRepository::class);

        $this->assertInstanceOf(CommitRepository::class, $repo);
        $this->assertInstanceOf(CommitRepoContract::class, $repo);
    }

    /** @test */
    public function testModelBinding()
    {
        // setup
        factory(Commit::class, 3)->create();

        // act
        /** @var CommitRepository $repo */
        $repo = app(CommitRepository::class);
        $result = $repo->get();

        // assert
        $this->assertCount(3, $result);
        foreach ($result as $model) {
            $this->assertInstanceOf(Commit::class, $model);
        }
    }
}
