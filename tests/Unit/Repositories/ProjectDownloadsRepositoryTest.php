<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Repositories\ProjectDownloadRepoContract;
use App\Models\ProjectDownload;
use App\Repositories\ProjectDownloadRepository;
use Tests\TestCase;

class ProjectDownloadsRepositoryTest extends TestCase
{
    /** @test */
    public function testInstantiate()
    {
        $repo = app(ProjectDownloadRepository::class);

        $this->assertInstanceOf(ProjectDownloadRepository::class, $repo);
        $this->assertInstanceOf(ProjectDownloadRepoContract::class, $repo);
    }

    /** @test */
    public function testModelBinding()
    {
        // setup
        factory(ProjectDownload::class, 3)->create();

        // act
        /** @var ProjectDownloadRepository $repo */
        $repo = app(ProjectDownloadRepository::class);
        $result = $repo->get();

        // assert
        $this->assertCount(3, $result);
        foreach ($result as $model) {
            $this->assertInstanceOf(ProjectDownload::class, $model);
        }
    }
}
