<?php

namespace Tests\Unit\Repositories;

use App\Models\Author;
use App\Repositories\AuthorRepository;
use Illuminate\Support\Collection;
use Tests\TestCase;

class CommonRepositoryTest extends TestCase
{
    const TEST_CLASS = Author::class;
    const TEST_REPO  = AuthorRepository::class;

    /**
     * @var AuthorRepository
     */
    protected $repo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repo = app(static::TEST_REPO);
    }

    /** @test */
    public function testFilterByName()
    {
        // setup
        factory(static::TEST_CLASS)->create(['name' => 'Project1']);
        factory(static::TEST_CLASS, 3)->create();

        // act
        /** @var Author|Collection $result */
        $result = $this->repo->filterByName('Project1')
                             ->get();

        // assert
        $this->assertCount(1, $result);
        $this->assertEquals('Project1', $result->first()->name);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testWithDeleted()
    {
        // setup
        factory(static::TEST_CLASS)->create(['name' => 'Project1'])->delete();

        // pre assert
        $this->assertEquals(0, app(static::TEST_CLASS)->count());

        // act
        /** @var Author|Collection $result */
        $result = $this->repo->withDeleted()
                             ->get();

        // assert
        $this->assertCount(1, $result);
        $this->assertEquals('Project1', $result->first()->name);
    }

    /**
     * @test
     * @throws \Exception
     */
    public function testFilterByNameWithDeleted()
    {
        // setup
        factory(static::TEST_CLASS)->create(['name' => 'Project1'])->delete();
        factory(static::TEST_CLASS)->create(['name' => 'Project2'])->delete();

        // pre assert
        $this->assertEquals(0, app(static::TEST_CLASS)->count());

        // act
        /** @var Author|Collection $result */
        $result = $this->repo->filterByName('Project1')
                             ->withDeleted()
                             ->get();

        // assert
        // assert
        $this->assertCount(1, $result);
        $this->assertEquals('Project1', $result->first()->name);
    }
}
