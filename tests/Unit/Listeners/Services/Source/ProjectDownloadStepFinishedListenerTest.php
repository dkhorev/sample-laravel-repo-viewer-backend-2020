<?php

namespace Tests\Unit\Listeners\Services\Source;

use App\Constants\ProjectDownloadsStatus;
use App\Events\Services\Source\ProjectDownloadFinishedEvent;
use App\Events\Services\Source\ProjectDownloadStepFinishedEvent;
use App\Listeners\Services\Source\ProjectDownloadStepFinishedListener;
use App\Models\ProjectDownload;
use App\Services\Source\GithubFetchService;
use Illuminate\Support\Facades\Event;
use Mockery\MockInterface;
use Tests\TestCase;

class ProjectDownloadStepFinishedListenerTest extends TestCase
{
    /** @test */
    public function testEventWhenDownloadJobFinished()
    {
        // setup
        Event::fake();
        $job = factory(ProjectDownload::class)->create(['status' => ProjectDownloadsStatus::STATUS_FINISHED])
                                              ->fresh();

        /** @var ProjectDownloadStepFinishedListener $listener */
        $listener = app(ProjectDownloadStepFinishedListener::class);

        // act
        $listener->handle(new ProjectDownloadStepFinishedEvent($job));

        // assert
        Event::assertDispatched(ProjectDownloadFinishedEvent::class, 1);
    }

    /** @test */
    public function testNextPageWhenJobInProgress()
    {
        // setup
        Event::fake();
        /** @var ProjectDownload $job */
        $job = factory(ProjectDownload::class)->create(['status' => ProjectDownloadsStatus::STATUS_PROGRESS])
                                              ->fresh();
        // mock | pre assert
        $this->mock(GithubFetchService::class, function (MockInterface $mock) use ($job) {
            $mock->shouldReceive('nextPage')
                 ->with($job)
                 ->andReturn(true);
        });

        /** @var ProjectDownloadStepFinishedListener $listener */
        $listener = app(ProjectDownloadStepFinishedListener::class);

        // act
        $listener->handle(new ProjectDownloadStepFinishedEvent($job));

        // assert
        Event::assertNotDispatched(ProjectDownloadFinishedEvent::class);
    }
}
