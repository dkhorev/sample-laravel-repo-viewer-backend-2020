<?php

use App\Http\Controllers\RepositoryController;

Route::group([
    'prefix' => 'v1/repo',
], function () {
    Route::get('/', [RepositoryController::class, 'index'])->name('repo.index');
    Route::post('/', [RepositoryController::class, 'import'])->name('repo.import');
    Route::get('/{project}/commits', [RepositoryController::class, 'commits'])->name('repo.project.commits');
    Route::delete('/{project}/commits', [RepositoryController::class, 'delete'])->name('repo.delete');
});
