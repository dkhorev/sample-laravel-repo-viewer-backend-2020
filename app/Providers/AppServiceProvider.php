<?php

namespace App\Providers;

use App\Contracts\Services\Commit\DeleteCommitsServiceContract;
use App\Contracts\Services\Project\ListCommitsServiceContract;
use App\Contracts\Services\Project\ListProjectsServiceContract;
use App\Contracts\Services\Project\ProjectAddServiceContract;
use App\Contracts\Services\Source\ImportServiceContract;
use App\Services\Commit\DeleteCommitsService;
use App\Services\Project\ListCommitsService;
use App\Services\Project\ListProjectsService;
use App\Services\Project\ProjectAddService;
use App\Services\Source\ImportService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ClientInterface::class, Client::class);
        $this->app->bind(ImportServiceContract::class, ImportService::class);
        $this->app->bind(ProjectAddServiceContract::class, ProjectAddService::class);
        $this->app->bind(ListProjectsServiceContract::class, ListProjectsService::class);
        $this->app->bind(ListCommitsServiceContract::class, ListCommitsService::class);
        $this->app->bind(DeleteCommitsServiceContract::class, DeleteCommitsService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
