<?php

namespace App\Providers;

use App\Contracts\Repositories\AuthorRepoContract;
use App\Contracts\Repositories\CommitRepoContract;
use App\Contracts\Repositories\ProjectDownloadRepoContract;
use App\Contracts\Repositories\ProjectRepoContract;
use App\Repositories\AuthorRepository;
use App\Repositories\CommitRepository;
use App\Repositories\ProjectDownloadRepository;
use App\Repositories\ProjectRepository;
use Illuminate\Support\ServiceProvider;

class AppRepositoryProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthorRepoContract::class, AuthorRepository::class);
        $this->app->bind(ProjectRepoContract::class, ProjectRepository::class);
        $this->app->bind(CommitRepoContract::class, CommitRepository::class);
        $this->app->bind(ProjectDownloadRepoContract::class, ProjectDownloadRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
