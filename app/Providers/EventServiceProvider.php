<?php

namespace App\Providers;

use App\Events\Services\Source\ProjectDownloadStepFinishedEvent;
use App\Listeners\Services\Source\ProjectDownloadStepFinishedListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ProjectDownloadStepFinishedEvent::class => [
            ProjectDownloadStepFinishedListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
