<?php

namespace App\Console\Commands;

use App\Contracts\Repositories\AuthorRepoContract;
use App\Contracts\Repositories\ProjectRepoContract;
use App\Models\Author;
use App\Models\Commit;
use App\Models\Project;
use App\Rules\ValidRepoAliasRule;
use Illuminate\Console\Command;

class ListCommits extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repo:show {repoAlias}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show commits for specific author/repo';

    /**
     * @var AuthorRepoContract
     */
    protected $authorRepo;

    /**
     * @var ProjectRepoContract
     */
    protected $projectRepo;

    /**
     * Create a new command instance.
     *
     * @param AuthorRepoContract  $authorRepo
     * @param ProjectRepoContract $projectRepo
     */
    public function __construct(AuthorRepoContract $authorRepo, ProjectRepoContract $projectRepo)
    {
        parent::__construct();
        $this->authorRepo = $authorRepo;
        $this->projectRepo = $projectRepo;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        /** @var ValidRepoAliasRule $rule */
        $rule = app(ValidRepoAliasRule::class);

        $repoAlias = $this->argument('repoAlias');

        if ($rule->passes('', $repoAlias)) {
            [$author, $project] = explode('/', $repoAlias);

            /** @var Author $author */
            $author = $this->authorRepo->filterByName($author)
                                       ->get()
                                       ->first();

            if ($author) {
                /** @var Project $project */
                $project = $this->projectRepo->filterByName($project)
                                             ->filterByAuthorId($author->id)
                                             ->get()
                                             ->first();

                if ($project) {
                    // todo may impelent formatter pattern for presentations
                    $this->info("Repository {$author->name}/{$project->name}");
                    $this->info("Commits count: {$project->commits->count()}");

                    $project->commits->each(function (Commit $commit) {
                        $this->info("{$commit->sha}");
                    });
                } else {
                    $this->error('Repository not found.');
                }
            } else {
                $this->error('Author not found.');
            }
        }
    }
}
