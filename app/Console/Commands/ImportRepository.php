<?php

namespace App\Console\Commands;

use App\Contracts\Services\Project\ProjectAddServiceContract;
use App\Rules\ValidRepoAliasRule;
use App\Services\Source\GithubFetchService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Throwable;

class ImportRepository extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repo:import {repoAlias}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import author/repo';

    /**
     * @var ProjectAddServiceContract
     */
    protected $projectAddService;

    /**
     * Create a new command instance.
     *
     * @param ProjectAddServiceContract $projectAddService
     */
    public function __construct(ProjectAddServiceContract $projectAddService)
    {
        parent::__construct();
        $this->projectAddService = $projectAddService;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws GuzzleException
     * @throws Throwable
     */
    public function handle()
    {
        /** @var ValidRepoAliasRule $rule */
        $rule = app(ValidRepoAliasRule::class);

        $repoAlias = $this->argument('repoAlias');

        if ($rule->passes('', $repoAlias)) {
            [$author, $project] = explode('/', $repoAlias);

            $result = $this->projectAddService->add(app(GithubFetchService::class), $author, $project);

            if ($result) {
                $this->info('Done!');
            } else {
                $this->error('Error. Not found or private.');
            }
        }
    }
}
