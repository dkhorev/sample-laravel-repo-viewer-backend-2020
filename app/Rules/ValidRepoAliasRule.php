<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidRepoAliasRule implements Rule
{
    /**
     * @inheritDoc
     */
    public function passes($attribute, $value)
    {
        if (is_string($value)) {
            $data = explode('/', $value);

            return count($data) === 2 && strlen($data[0]) && strlen($data[1]);
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function message()
    {
        return __('validation.app.invalid_repo_alias');
    }
}
