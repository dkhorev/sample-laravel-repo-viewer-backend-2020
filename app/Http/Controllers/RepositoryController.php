<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\CommitRepoContract;
use App\Contracts\Repositories\ProjectRepoContract;
use App\Contracts\Services\Commit\DeleteCommitsServiceContract;
use App\Contracts\Services\Project\ProjectAddServiceContract;
use App\Http\Requests\DeleteCommitsRequest;
use App\Http\Requests\RepositoryImportRequest;
use App\Http\Resources\CommitsListCollection;
use App\Http\Resources\RepositoryListCollection;
use App\Models\Project;
use App\Services\Source\GithubFetchService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * Class RepositoryController
 *
 * @package App\Http\Controllers
 */
class RepositoryController extends Controller
{
    /**
     * Default pagination
     */
    const PER_PAGE = 20;

    /**
     * @param ProjectRepoContract $projectRepo
     * @param Request             $request
     *
     * @return JsonResource
     */
    public function index(ProjectRepoContract $projectRepo, Request $request)
    {
        $resource = $projectRepo->orderByDesc('created_at')
                                ->paginate($request->per_page ?? static::PER_PAGE);

        return app(RepositoryListCollection::class, ['resource' => $resource]);
    }

    /**
     * @param CommitRepoContract $commitRepo
     * @param Request            $request
     * @param Project            $project
     *
     * @return JsonResource
     */
    public function commits(CommitRepoContract $commitRepo, Request $request, Project $project)
    {
        $resource = $commitRepo->filterByProject($project)
                               ->orderByDesc('created_at')
                               ->paginate($request->per_page ?? static::PER_PAGE);

        return app(CommitsListCollection::class, ['resource' => $resource]);
    }

    /**
     * @param RepositoryImportRequest   $request
     * @param ProjectAddServiceContract $projectAddService
     *
     * @return JsonResponse
     */
    public function import(RepositoryImportRequest $request, ProjectAddServiceContract $projectAddService)
    {
        try {
            [$author, $project] = explode('/', $request->alias);

            // todo *FetchService can be instantiated by request property [not implemented for demo]
            $result = $projectAddService->add(app(GithubFetchService::class), $author, $project);

            if ($result) {
                return response()->json([], Response::HTTP_CREATED);
            }

            return response()->json([
                'message' => __('repo.not_found.or.private'),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (Throwable $e) {
            Log::error($e->getMessage());

            return response()->json([
                'message' => __('fetch_service.error'),
            ], Response::HTTP_CONFLICT);
        }
    }

    /**
     * @param DeleteCommitsRequest         $request
     * @param DeleteCommitsServiceContract $deleteCommitsService
     * @param Project                      $project
     *
     * @return JsonResponse
     */
    public function delete(
        DeleteCommitsRequest $request,
        DeleteCommitsServiceContract $deleteCommitsService,
        Project $project
    ) {
        $deleteCommitsService->deleteMany($project->id, $request->commits);

        return response()->json([], Response::HTTP_OK);
    }
}
