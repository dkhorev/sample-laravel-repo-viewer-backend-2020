<?php

namespace App\Http\Rules;

use App\Rules\ValidRepoAliasRule;
use Illuminate\Contracts\Validation\Rule;

class ValidRepoAliasHttpRule implements Rule
{
    /**
     * @var ValidRepoAliasRule
     */
    private $rule;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->rule = app(ValidRepoAliasRule::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->rule->passes($attribute, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->rule->message();
    }
}
