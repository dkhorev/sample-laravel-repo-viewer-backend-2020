<?php

namespace App\Http\Requests;

use App\Http\Rules\ValidRepoAliasHttpRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RepositoryImportRequest
 *
 * @property-read string $alias
 * @package App\Http\Requests
 */
class RepositoryImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'alias' => [
                'string',
                'required',
                app(ValidRepoAliasHttpRule::class),
            ],
        ];
    }
}
