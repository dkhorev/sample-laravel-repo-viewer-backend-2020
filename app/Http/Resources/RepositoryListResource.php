<?php

namespace App\Http\Resources;

use App\Models\Project;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class RepositoryListResource
 * @property Project $resource
 * @package App\Http\Resources
 */
class RepositoryListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->resource->id,
            'name'          => $this->resource->name,
            'author'        => $this->resource->author->name,
            'commits_count' => $this->resource->commits->count(),
        ];
    }
}
