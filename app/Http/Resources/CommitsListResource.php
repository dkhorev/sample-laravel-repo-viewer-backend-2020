<?php

namespace App\Http\Resources;

use App\Models\Commit;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CommitsListResource
 *
 * @property Commit $resource
 * @package App\Http\Resources
 */
class CommitsListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'  => $this->resource->id,
            'sha' => $this->resource->sha,
        ];
    }
}
