<?php

namespace App\Repositories;

use App\Contracts\Repositories\ProjectRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Project;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectRepository
 *
 * @package App\Repositories
 */
class ProjectRepository extends CommonRepository implements ProjectRepoContract
{
    /**
     * @inheritDoc
     */
    protected function init(): RepoContract
    {
        $this->model = Project::with([
            'author',
            'commits'
        ]);

        return $this;
    }

    /**
     * Create new model
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data): Model
    {
        $this->reset();

        $project = $this->model->withTrashed()->firstOrCreate(
            [
                'name' => $data['name'],
            ],
            $data
        );

        if ($project->trashed()) {
            $project->restore();
        }

        return $project;
    }

    /**
     * @param int $id
     *
     * @return ProjectRepoContract
     */
    public function filterByAuthorId(int $id): ProjectRepoContract
    {
        $this->model->where('author_id', $id);

        return $this;
    }
}
