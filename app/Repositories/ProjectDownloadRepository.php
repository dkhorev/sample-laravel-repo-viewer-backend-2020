<?php

namespace App\Repositories;

use App\Contracts\Repositories\ProjectDownloadRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\ProjectDownload;

class ProjectDownloadRepository extends AbstractMysqlRepository implements ProjectDownloadRepoContract
{
    /**
     * @inheritDoc
     */
    protected function init(): RepoContract
    {
        $this->model = ProjectDownload::with([
            'project',
        ]);

        return $this;
    }
}
