<?php

namespace App\Repositories;

use App\Contracts\Repositories\AuthorRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Author;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AuthorRepository
 *
 * @package App\Repositories
 */
class AuthorRepository extends CommonRepository implements AuthorRepoContract
{
    /**
     * @inheritDoc
     */
    protected function init(): RepoContract
    {
        $this->model = Author::with([
            'projects',
        ]);

        return $this;
    }

    /**
     * Create new model
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data): Model
    {
        $this->reset();

        $author = $this->model->withTrashed()->firstOrCreate(
            [
                'name' => $data['name'],
            ],
            $data
        );

        if ($author->trashed()) {
            $author->restore();
        }

        return $author;
    }
}
