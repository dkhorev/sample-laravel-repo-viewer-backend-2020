<?php

namespace App\Repositories;

use App\Contracts\Repositories\Partial\CommonRepoContract;

abstract class CommonRepository extends AbstractMysqlRepository implements CommonRepoContract
{
    /**
     * @inheritDoc
     */
    public function filterByName(string $name): CommonRepoContract
    {
        $this->model->where('name', $name);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function withDeleted(): CommonRepoContract
    {
        $this->model->withTrashed();

        return $this;
    }
}
