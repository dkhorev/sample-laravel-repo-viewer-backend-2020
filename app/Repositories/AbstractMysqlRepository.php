<?php

namespace App\Repositories;

use App\Contracts\Repositories\RepoContract;
use App\Models\BaseModel;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class AbstractMysqlRepository implements RepoContract
{
    /**
     * @var Model|Builder|BaseModel
     */
    protected $model;

    /**
     * Repo constructor
     *
     * Init empty builder
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Set default builder to model property
     *
     * @return mixed
     */
    abstract protected function init(): RepoContract;

    /**
     * Reset repo filters
     *
     * @return RepoContract
     */
    public function reset(): RepoContract
    {
        return $this->init();
    }

    /**
     * Take the results
     *
     * @return Collection|static[]
     */
    public function get(): Collection
    {
        return $this->model->get();
    }

    /**
     * Retrieve instance model by Id
     *
     * @param $id
     *
     * @return Model
     */
    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Create new model
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data): Model
    {
        return $this->model->create($data);
    }

    /**
     * Delete model
     *
     * @param Model $model
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(Model $model): bool
    {
        return $this->model->delete($model);
    }

    /**
     * @param string $field
     *
     * @return RepoContract
     */
    public function orderByDesc(string $field): RepoContract
    {
        $this->model->orderByDesc($field);

        return $this;
    }

    /**
     * @param int $perPage
     *
     * @return LengthAwarePaginator
     */
    public function paginate(int $perPage): LengthAwarePaginator
    {
        return $this->model->paginate($perPage);
    }
}
