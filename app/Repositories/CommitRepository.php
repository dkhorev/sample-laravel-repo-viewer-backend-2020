<?php

namespace App\Repositories;

use App\Contracts\Repositories\CommitRepoContract;
use App\Contracts\Repositories\RepoContract;
use App\Models\Commit;
use App\Models\Project;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CommitRepository
 *
 * @package App\Repositories
 */
class CommitRepository extends CommonRepository implements CommitRepoContract
{
    /**
     * @inheritDoc
     */
    protected function init(): RepoContract
    {
        $this->model = Commit::with([
            'project',
        ]);

        return $this;
    }

    /**
     * Create new model
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data): Model
    {
        $this->reset();

        $commit = $this->model->withTrashed()->firstOrCreate(
            [
                'sha'        => $data['sha'],
                'project_id' => $data['project_id'],
            ],
            $data
        );

        if ($commit->trashed()) {
            $commit->restore();
        }

        return $commit;
    }

    /**
     * @param Project $project
     *
     * @return CommitRepoContract
     */
    public function filterByProject(Project $project): CommitRepoContract
    {
        $this->model->where('project_id', $project->id);

        return $this;
    }

    /**
     * @param int $id
     *
     * @return CommitRepoContract
     */
    public function filterByProjectId(int $id): CommitRepoContract
    {
        $this->model->where('project_id', $id);

        return $this;
    }

    /**
     * @param array $commits
     *
     * @return CommitRepoContract
     */
    public function filterByCommitsArray(array $commits): CommitRepoContract
    {
        $this->model->whereIn('id', $commits);

        return $this;
    }
}
