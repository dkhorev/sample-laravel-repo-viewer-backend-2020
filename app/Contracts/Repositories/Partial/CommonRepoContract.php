<?php

namespace App\Contracts\Repositories\Partial;

use App\Contracts\Repositories\RepoContract;

/**
 * Interface CommonRepoContract
 * Common use partial interface
 *
 * @package App\Contracts\Repositories\Partial
 */
interface CommonRepoContract extends RepoContract
{
    /**
     * @param string $name
     *
     * @return $this
     */
    public function filterByName(string $name): CommonRepoContract;

    /**
     * Show with deleted entries
     *
     * @return $this
     */
    public function withDeleted(): CommonRepoContract;
}
