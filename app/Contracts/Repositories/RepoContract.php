<?php

namespace App\Contracts\Repositories;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Interface RepoContract
 *
 * @package App\Contracts\Repositories
 */
interface RepoContract
{
    /**
     * Get results
     *
     * @return Collection|RepoContract[]
     */
    public function get(): Collection;

    /**
     * Reset all filters
     *
     * @return RepoContract
     */
    public function reset(): RepoContract;

    /**
     * Retrieve instance model by Id
     *
     * @param $id
     *
     * @return Model
     */
    public function findById($id);

    /**
     * Create new model
     *
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data): Model;

    /**
     * Delete model
     *
     * @param Model $model
     *
     * @return bool
     */
    public function delete(Model $model): bool;

    /**
     * @param string $field
     *
     * @return RepoContract
     */
    public function orderByDesc(string $field): RepoContract;

    /**
     * @param int $perPage
     *
     * @return LengthAwarePaginator
     */
    public function paginate(int $perPage): LengthAwarePaginator;
}
