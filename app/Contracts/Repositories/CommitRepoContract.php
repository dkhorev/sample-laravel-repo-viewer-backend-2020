<?php

namespace App\Contracts\Repositories;

use App\Contracts\Repositories\Partial\CommonRepoContract;
use App\Models\Project;

interface CommitRepoContract extends CommonRepoContract
{
    public function filterByProject(Project $project): CommitRepoContract;
    public function filterByProjectId(int $id): CommitRepoContract;
    public function filterByCommitsArray(array $commits): CommitRepoContract;
}
