<?php

namespace App\Contracts\Repositories;

use App\Contracts\Repositories\Partial\CommonRepoContract;

interface ProjectRepoContract extends CommonRepoContract
{
    public function filterByAuthorId(int $id): ProjectRepoContract;
}
