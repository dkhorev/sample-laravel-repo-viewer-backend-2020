<?php

namespace App\Contracts\Services\Source;

use App\Exceptions\Services\Source\CommitsImportFailedException;
use App\Exceptions\Services\Source\NextPageStatusNotSetException;
use App\Models\ProjectDownload;
use Throwable;

interface ImportServiceContract
{
    /**
     * @param bool $hasNextPage
     *
     * @return $this
     */
    public function setHasNextPage(bool $hasNextPage): self;

    /**
     * @param string $author
     * @param string $project
     * @param array  $commits
     *
     * @return ProjectDownload
     * @throws NextPageStatusNotSetException
     * @throws CommitsImportFailedException
     * @throws Throwable
     */
    public function importNew(string $author, string $project, array $commits): ProjectDownload;

    /**
     * @param ProjectDownload $job
     * @param array           $commits
     *
     * @return ProjectDownload
     * @throws NextPageStatusNotSetException
     * @throws CommitsImportFailedException
     * @throws Throwable
     */
    public function importForJob(ProjectDownload $job, array $commits): ProjectDownload;
}
