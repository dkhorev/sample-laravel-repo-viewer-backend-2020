<?php

namespace App\Contracts\Services\Source;

use App\Models\ProjectDownload;
use GuzzleHttp\Exception\GuzzleException;
use Throwable;

interface FetchServiceContract
{
    /**
     * Try initial project download and create download job on success
     *
     * @param string $author
     * @param string $project
     *
     * @return bool
     * @throws GuzzleException
     * @throws Throwable
     */
    public function initProjectDownload(string $author, string $project): bool;

    /**
     * Fetch next page for job
     *
     * @param ProjectDownload $job
     *
     * @return bool
     * @throws GuzzleException
     * @throws Throwable
     */
    public function nextPage(ProjectDownload $job): bool;
}
