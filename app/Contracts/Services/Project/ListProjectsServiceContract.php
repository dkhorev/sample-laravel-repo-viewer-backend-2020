<?php

namespace App\Contracts\Services\Project;

use Illuminate\Support\Collection;

interface ListProjectsServiceContract
{
    /**
     * Collection of projects (repos)
     *
     * @return Collection
     */
    public function get(): Collection;
}
