<?php

namespace App\Contracts\Services\Project;

use App\Contracts\Services\Source\FetchServiceContract;
use GuzzleHttp\Exception\GuzzleException;
use Throwable;

interface ProjectAddServiceContract
{
    /**
     * Adds download job or returns one in progress
     *
     * @param FetchServiceContract $service
     * @param string               $author
     * @param string               $project
     *
     * @return bool
     *
     * @throws Throwable
     * @throws GuzzleException
     */
    public function add(FetchServiceContract $service, string $author, string $project): bool;
}
