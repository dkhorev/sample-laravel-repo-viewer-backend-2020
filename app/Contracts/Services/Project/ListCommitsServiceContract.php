<?php

namespace App\Contracts\Services\Project;

use App\Models\Project;
use Illuminate\Support\Collection;

interface ListCommitsServiceContract
{
    /**
     * Collection of commits for project (repos)
     *
     * @param Project $project
     *
     * @return Collection
     */
    public function getForProject(Project $project): Collection;
}
