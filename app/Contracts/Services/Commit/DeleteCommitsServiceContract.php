<?php

namespace App\Contracts\Services\Commit;

interface DeleteCommitsServiceContract
{
    /**
     * Delete many commits by their IDs
     *
     * @param int   $projectId
     * @param array $commits
     *
     * @return bool
     */
    public function deleteMany(int $projectId, array $commits): bool;
}
