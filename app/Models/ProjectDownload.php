<?php

namespace App\Models;

use App\Constants\ProjectDownloadsStatus;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class ProjectDownloadEvent
 *
 * @property int     $id
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 * @property int     $project_id
 * @property int     $pages_parsed
 * @property string  $status
 * @property Project $project
 * @package  App\Models
 */
class ProjectDownload extends BaseModel
{
    protected $fillable = [
        'project_id',
        'pages_parsed',
        'status',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'update_at',
    ];

    /**
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * todo must be contract method
     *
     * @return bool
     */
    public function isFinished(): bool
    {
        return $this->status === ProjectDownloadsStatus::STATUS_FINISHED;
    }

    /**
     * todo must be contract method
     *
     * @return bool
     */
    public function isProgress(): bool
    {
        return !$this->isFinished();
    }
}
