<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class Project
 *
 * @property int               $id
 * @property string            $name
 * @property Carbon            $created_at
 * @property Carbon            $updated_at
 * @property Carbon            $deleted_at
 * @property int               $author_id
 * @property Author            $author
 * @property Commit|Collection $commits
 * @package App\Models
 */
class Project extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'author_id',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'update_at',
        'deleted_at',
    ];

    /**
     * @return BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    /**
     * @return HasMany
     */
    public function commits()
    {
        return $this->hasMany(Commit::class);
    }
}
