<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 * @method static Builder|BaseModel withTrashed()
 * @method static Model firstOrCreate()
 * @method static bool trashed()
 * @method static bool restore()
 *
 * @package  App\Models
 * @mixin Eloquent
 */
abstract class BaseModel extends Model
{

}
