<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class Author
 *
 * @property int                $id
 * @property string             $name
 * @property Carbon             $created_at
 * @property Carbon             $updated_at
 * @property Carbon             $deleted_at
 * @property Project|Collection $projects
 * @package  App\Models
 */
class Author extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'update_at',
        'deleted_at',
    ];

    /**
     * @return HasMany
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
