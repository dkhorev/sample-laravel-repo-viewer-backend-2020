<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class Commit
 *
 * @property int     $id
 * @property string  $sha
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 * @property Carbon  $deleted_at
 * @property int     $project_id
 * @property Project $project
 * @package  App\Models
 */
class Commit extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'sha',
        'project_id',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'created_at',
        'update_at',
        'deleted_at',
    ];

    /**
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
