<?php

namespace App\Events\Services\Source;

use App\Models\ProjectDownload;
use Illuminate\Queue\SerializesModels;

abstract class ProjectDownloadEvent
{
    use SerializesModels;

    /**
     * @var ProjectDownload
     */
    public $downloadJob;

    /**
     * Create a new event instance.
     *
     * @param ProjectDownload $job
     */
    public function __construct(ProjectDownload $job)
    {
        $this->downloadJob = $job;
    }
}
