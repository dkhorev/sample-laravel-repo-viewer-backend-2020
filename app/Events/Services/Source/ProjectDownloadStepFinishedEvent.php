<?php

namespace App\Events\Services\Source;

/**
 * Class ProjectDownloadStepFinishedEvent
 *
 * @package App\Events\Services\Source
 */
class ProjectDownloadStepFinishedEvent extends ProjectDownloadEvent
{

}
