<?php

namespace App\Services\Source;

use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;

class GithubFetchService extends AbstractFetchService
{
    /**
     * Get path for next page request for service
     *
     * @return string
     */
    protected function getPath(): string
    {
        $path = config('app.fetch_service.url_template.github');
        $path = str_replace(':author', $this->author, $path);
        $path = str_replace(':project', $this->project, $path);

        return $path;
    }

    /**
     * Validates if service response has OK status
     *
     * @param ResponseInterface $response
     *
     * @return bool
     */
    protected function isOkResponse(ResponseInterface $response): bool
    {
        return $response->getStatusCode() === Response::HTTP_OK
            || $response->getStatusCode() === Response::HTTP_CONFLICT;
    }

    /**
     * Checks if project has next page
     *
     * @param ResponseInterface $response
     *
     * @return bool
     */
    protected function hasNextPage(ResponseInterface $response): bool
    {
        $header = $response->getHeader('Link');

        if (count($header) === 0) {
            return false;
        }

        preg_match('/rel="next"/m', $header[0], $matches);

        return count($matches) === 1;
    }
}
