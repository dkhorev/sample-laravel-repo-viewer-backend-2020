<?php

namespace App\Services\Source;

use App\Contracts\Services\Source\FetchServiceContract;
use App\Contracts\Services\Source\ImportServiceContract;
use App\Events\Services\Source\ProjectDownloadStepFinishedEvent;
use App\Models\ProjectDownload;
use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;
use Throwable;

abstract class AbstractFetchService implements FetchServiceContract
{
    /**
     * Succesfull step finish event
     *
     * @var string
     */
    protected $event = ProjectDownloadStepFinishedEvent::class;

    /**
     * Current page for parsing
     *
     * @var null|int
     */
    protected $page = null;

    /**
     * @var null|string
     */
    protected $author = null;

    /**
     * @var null|string
     */
    protected $project = null;

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var ImportServiceContract
     */
    protected $importService;

    /**
     * @var ProjectDownload|null
     */
    private $job;

    /**
     * Get path for next page request for service
     *
     * @return string
     */
    abstract protected function getPath(): string;

    /**
     * Validates if service response has OK status
     *
     * @param ResponseInterface $response
     *
     * @return bool
     */
    abstract protected function isOkResponse(ResponseInterface $response): bool;

    /**
     * Checks if project has next page
     *
     * @param ResponseInterface $response
     *
     * @return bool
     */
    abstract protected function hasNextPage(ResponseInterface $response): bool;

    /**
     * AbstractFetchService constructor.
     *
     * @param ClientInterface       $client
     * @param ImportServiceContract $importService
     */
    public function __construct(ClientInterface $client, ImportServiceContract $importService)
    {
        $this->client = $client;
        $this->importService = $importService;
    }

    /**
     * Try initial project download and create download job on success
     *
     * @param string $author
     * @param string $project
     *
     * @return bool
     * @throws GuzzleException
     * @throws Throwable
     */
    public function initProjectDownload(string $author, string $project): bool
    {
        $this->setAuthor($author)
             ->setProject($project);

        // todo validate repo and author not null
        try {
            $response = $this->getResponse();

            if ($this->isOkResponse($response)) {
                $this->importResponse($response);
                return true;
            }
        } catch (Exception $e) {
            Log::error($e->getMessage(), ['in' => __METHOD__]);
        }

        return false;
    }

    /**
     * @param string $author
     *
     * @return $this
     */
    private function setAuthor(string $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @param string $project
     *
     * @return $this
     */
    private function setProject(string $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return ResponseInterface
     * @throws GuzzleException
     */
    private function getResponse(): ResponseInterface
    {
        return $this->client->request(
            'get',
            $this->getPath(),
            array_merge(config('app.fetch_service.http_client_options'), [
                'query' => [
                    'per_page' => config('app.fetch_service.per_page'),
                    'page'     => $this->page ?? 1,
                ],
            ])
        );
    }

    /**
     * @param ResponseInterface $response
     *
     * @throws Throwable
     */
    protected function importResponse(ResponseInterface $response): void
    {
        $commits = json_decode($response->getBody()->getContents(), true);

        $this->importService->setHasNextPage($this->hasNextPage($response));

        if ($this->job) {
            $this->job = $this->importService->importForJob($this->job, $commits);
        } else {
            $this->job = $this->importService->importNew($this->author, $this->project, $commits);
        }

        event(app($this->event, ['job' => $this->job]));
    }

    /**
     * Fetch next page for job
     *
     * @param ProjectDownload $job
     *
     * @return bool
     * @throws GuzzleException
     * @throws Throwable
     */
    public function nextPage(ProjectDownload $job): bool
    {
        $this->setAuthor($job->project->author->name)
             ->setProject($job->project->name)
             ->setNextPage($job);

        try {
            $response = $this->getResponse();

            if ($this->isOkResponse($response)) {
                $this->setJob($job)
                     ->importResponse($response);

                return true;
            }
        } catch (Exception $e) {
            Log::error($e->getMessage(), ['in' => __METHOD__]);
        }

        return false;
    }

    /**
     * @param ProjectDownload $job
     *
     * @return $this
     */
    private function setNextPage(ProjectDownload $job)
    {
        $this->page = $job->pages_parsed + 1;

        return $this;
    }

    /**
     * @param ProjectDownload $job
     *
     * @return $this
     */
    private function setJob(ProjectDownload $job)
    {
        $this->job = $job;

        return $this;
    }
}
