<?php

namespace App\Services\Source;

use App\Constants\ProjectDownloadsStatus;
use App\Contracts\Repositories\AuthorRepoContract;
use App\Contracts\Repositories\CommitRepoContract;
use App\Contracts\Repositories\ProjectDownloadRepoContract;
use App\Contracts\Repositories\ProjectRepoContract;
use App\Contracts\Services\Source\ImportServiceContract;
use App\Exceptions\Services\Source\CommitsImportFailedException;
use App\Exceptions\Services\Source\NextPageStatusNotSetException;
use App\Models\Author;
use App\Models\Project;
use App\Models\ProjectDownload;
use Illuminate\Support\Facades\DB;
use Throwable;

/**
 * Class ImportService
 * Responsible for importing data, for restoring soft deleted records
 * todo can be separated for 'create new' and 'update existing' services
 *
 * @package App\Services\Source
 */
class ImportService implements ImportServiceContract
{
    /**
     * @var CommitRepoContract
     */
    protected $commitRepo;

    /**
     * @var bool|null
     */
    private $hasNextPage;

    /**
     * @var ProjectRepoContract
     */
    private $projectRepo;

    /**
     * @var AuthorRepoContract
     */
    private $authorRepo;

    /**
     * @var ProjectDownloadRepoContract
     */
    private $jobRepo;

    /**
     * @var ProjectDownload|null
     */
    private $job;

    /**
     * ImportService constructor.
     *
     * @param AuthorRepoContract          $authorRepo
     * @param ProjectRepoContract         $projectRepo
     * @param CommitRepoContract          $commitRepo
     * @param ProjectDownloadRepoContract $jobRepo
     */
    public function __construct(
        AuthorRepoContract $authorRepo,
        ProjectRepoContract $projectRepo,
        CommitRepoContract $commitRepo,
        ProjectDownloadRepoContract $jobRepo
    ) {
        $this->projectRepo = $projectRepo;
        $this->authorRepo = $authorRepo;
        $this->jobRepo = $jobRepo;
        $this->commitRepo = $commitRepo;
    }

    /**
     * @param bool $hasNextPage
     *
     * @return ImportServiceContract
     */
    public function setHasNextPage(bool $hasNextPage): ImportServiceContract
    {
        $this->hasNextPage = $hasNextPage;

        return $this;
    }

    /**
     * @param string $author
     * @param string $project
     * @param array  $commits
     *
     * @return ProjectDownload
     * @throws NextPageStatusNotSetException
     * @throws Throwable
     */
    public function importNew(string $author, string $project, array $commits): ProjectDownload
    {
        $this->validateSetup();

        DB::transaction(function () use ($author, $project, $commits) {
            /** @var Author $author */
            $author = $this->authorRepo->create([
                'name' => $author,
            ]);

            /** @var Project $project */
            $project = $this->projectRepo->create([
                'name'      => $project,
                'author_id' => $author->id,
            ]);

            $this->createCommits($commits, $project);

            $this->job = $this->jobRepo->create([
                'project_id'   => $project->id,
                'status'       => $this->hasNextPage
                    ? ProjectDownloadsStatus::STATUS_PROGRESS
                    : ProjectDownloadsStatus::STATUS_FINISHED,
                'pages_parsed' => 1,
            ]);
        });

        if (is_null($this->job)) {
            throw new CommitsImportFailedException();
        }

        return $this->job;
    }

    /**
     * @return bool
     * @throws NextPageStatusNotSetException
     */
    private function validateSetup()
    {
        if (is_bool($this->hasNextPage)) {
            return true;
        }

        throw new NextPageStatusNotSetException('haxNextpage must be boolean');
    }

    /**
     * @param array   $commits
     * @param Project $project
     */
    protected function createCommits(array $commits, Project $project): void
    {
        foreach ($commits as $commit) {
            if (is_array($commit)) {
                $this->commitRepo->create([
                    'sha'        => $commit['sha'],
                    'project_id' => $project->id,
                ]);
            }
        }
    }

    /**
     * @param ProjectDownload $job
     * @param array           $commits
     *
     * @return ProjectDownload
     * @throws NextPageStatusNotSetException
     * @throws CommitsImportFailedException
     * @throws Throwable
     */
    public function importForJob(ProjectDownload $job, array $commits): ProjectDownload
    {
        $this->validateSetup();

        DB::transaction(function () use ($job, $commits) {
            $this->job = $job;

            $project = $this->job->project;

            $this->createCommits($commits, $project);

            $this->job->update([
                'status'       => $this->hasNextPage
                    ? ProjectDownloadsStatus::STATUS_PROGRESS
                    : ProjectDownloadsStatus::STATUS_FINISHED,
                'pages_parsed' => $this->job->pages_parsed + 1,
            ]);
        });

        if (is_null($this->job)) {
            throw new CommitsImportFailedException();
        }

        return $this->job;
    }
}
