<?php

namespace App\Services\Commit;

use App\Contracts\Repositories\CommitRepoContract;
use App\Contracts\Services\Commit\DeleteCommitsServiceContract;
use App\Models\Commit;
use Illuminate\Support\Facades\DB;
use Throwable;

class DeleteCommitsService implements DeleteCommitsServiceContract
{
    /**
     * @var CommitRepoContract
     */
    protected $commitRepo;

    /**
     * DeleteCommitsService constructor.
     *
     * @param CommitRepoContract $commitRepo
     */
    public function __construct(CommitRepoContract $commitRepo)
    {
        $this->commitRepo = $commitRepo;
    }

    /**
     * Delete many commits by their IDs
     *
     * @param int   $projectId
     * @param array $commits
     *
     * @return bool
     * @throws Throwable
     */
    public function deleteMany(int $projectId, array $commits): bool
    {
        $deleted = $this->commitRepo->filterByProjectId($projectId)
                                    ->filterByCommitsArray($commits)
                                    ->get();

        DB::transaction(function () use ($deleted) {
            $deleted->each(function (Commit $commit) {
                $commit->delete();
            });
        });

        return true;
    }
}
