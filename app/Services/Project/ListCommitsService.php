<?php

namespace App\Services\Project;

use App\Contracts\Repositories\ProjectRepoContract;
use App\Contracts\Services\Project\ListCommitsServiceContract;
use App\Models\Project;
use Illuminate\Support\Collection;

class ListCommitsService implements ListCommitsServiceContract
{
    /**
     * @var ProjectRepoContract
     */
    protected $projectRepo;

    /**
     * ListCommitsService constructor.
     *
     * @param ProjectRepoContract $projectRepo
     */
    public function __construct(ProjectRepoContract $projectRepo)
    {
        $this->projectRepo = $projectRepo;
    }

    /**
     * Collection of commits for project (repos)
     *
     * @param Project $project
     *
     * @return Collection
     */
    public function getForProject(Project $project): Collection
    {
        return $project->commits;
    }
}
