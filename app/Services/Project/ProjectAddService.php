<?php

namespace App\Services\Project;

use App\Contracts\Services\Project\ProjectAddServiceContract;
use App\Contracts\Services\Source\FetchServiceContract;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * Class ProjectAddService
 * Creates new download project by request data
 * todo should be async in real app
 *
 * @package App\Services\Project
 */
class ProjectAddService implements ProjectAddServiceContract
{
    /**
     * @var FetchServiceContract
     */
    private $fetchService;

    /**
     * Adds download job or returns one in progress
     *
     * @param FetchServiceContract $service
     * @param string               $author
     * @param string               $project
     *
     * @return bool
     * @throws GuzzleException
     * @throws Throwable
     */
    public function add(FetchServiceContract $service, string $author, string $project): bool
    {
        $this->fetchService = $service;

        try {
            return $this->fetchService->initProjectDownload($author, $project);
        } catch (Exception $e) {
            Log::error($e->getMessage(), ['in' => __METHOD__]);
            return false;
        }
    }
}
