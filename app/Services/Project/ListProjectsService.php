<?php

namespace App\Services\Project;

use App\Contracts\Repositories\ProjectRepoContract;
use App\Contracts\Services\Project\ListProjectsServiceContract;
use Illuminate\Support\Collection;

class ListProjectsService implements ListProjectsServiceContract
{
    /**
     * @var ProjectRepoContract
     */
    protected $projectRepoContract;

    /**
     * ListProjectsService constructor.
     *
     * @param ProjectRepoContract $projectRepoContract
     */
    public function __construct(ProjectRepoContract $projectRepoContract)
    {
        $this->projectRepoContract = $projectRepoContract;
    }

    /**
     * Collection of projects (repos)
     *
     * @return Collection
     */
    public function get(): Collection
    {
        return $this->projectRepoContract->get();
    }
}
