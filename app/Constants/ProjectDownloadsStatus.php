<?php

namespace App\Constants;

interface ProjectDownloadsStatus
{
    const STATUS_PROGRESS = 'progress';
    const STATUS_FINISHED = 'finished';
}
