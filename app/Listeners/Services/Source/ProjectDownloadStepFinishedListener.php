<?php

namespace App\Listeners\Services\Source;

use App\Contracts\Services\Source\FetchServiceContract;
use App\Events\Services\Source\ProjectDownloadFinishedEvent;
use App\Events\Services\Source\ProjectDownloadStepFinishedEvent;
use App\Services\Source\GithubFetchService;
use GuzzleHttp\Exception\GuzzleException;
use Throwable;

/**
 * Class ProjectDownloadStepFinishedListener
 * Responsible for validating each finished page download
 * Continues download sequence if job is not finished.
 *
 * @package App\Listeners\Services\Source
 */
class ProjectDownloadStepFinishedListener // implements ShouldQueue
{
    // use InteractsWithQueue;

    /**
     * Event when download is finished
     *
     * @var string
     */
    protected $event = ProjectDownloadFinishedEvent::class;

    /**
     * Handle the event.
     *
     * @param ProjectDownloadStepFinishedEvent $event
     *
     * @return void
     * @throws GuzzleException
     * @throws Throwable
     */
    public function handle(ProjectDownloadStepFinishedEvent $event)
    {
        $job = $event->downloadJob;

        if ($job->isFinished()) {
            event($this->event, ['job' => $job]);

            return;
        }

        // todo *FetchService can be instantiated by "project hasOne source" relation [not implemented for demo]
        if ($job->isProgress()) {
            /** @var FetchServiceContract $fetchService */
            $fetchService = app(GithubFetchService::class);
            $fetchService->nextPage($job);
        }
    }
}
