
### Models

Author
   
    +soft deletes
    +fields - id, name, created_at
    +hasMany - Project

Project
    
    +soft deletes
    +fields - id, name, author_id, created_at, deleted_at
    +belongsTo - Author
    +hasMany - Commit
    
Commit
    
    +soft deletes
    +fields - id, name, created_at, deleted_at, metadata(json) (to show message in front)
    +belongsTo - Project

ProjectDownloadEvent
    
    +fields - id, project_id, created_at, pages_parsed, status (enum)
    +belongsTo - Project

+Models relations tests, and factories

### Repos for models

abstract MysqlRepository
    
    +implements all standard methods
    +findById
    +*contract for: init method for model binding
    +*init method should add relations queries for each model
    +*contract for: create method
    +*contract for: delete method

abstract CommonRepository
    
    +filterByName
    +withDeleted

AuthorRepo + contract
    
    +test model binding
    
ProjectRepo + contract
    
    +test model binding
    
CommitRepo + contract
    
    +test model binding
    
ProjectDownloadsRepo + contract
    
    +withFinished

### Services for actions

GithubFetchService
    
    +can be part ob abstract and polymorphic class, ie we can grab gitlab, or bitbucket later
    +if repo is 0 commits set pages to 0 and finish
    +if repo is private or 404 set pages to 0 and throw error
    +contract with standard method
        setAuthor(...)
        setProject(...)
        get etc

ImportService + contract
    
    +importForJob(job, data): ProjectDownloadEvent - path one
    +importNew(author, project): ProjectDownloadEvent - path two
    +Checks if commit model exists in deleted and adds or restores in transaction

ProjectAddService
    
    +initiates a fetch service with correct method
    +ok with exceptions
    
event ProjectDownloadStepFinishedEvent +  
listener ProjectDownloadStepFinishedListener (sync)
    
    +If job FINISHED - fire finished parsing event
    +If job has PROGRESS status => fetch next page
    

ListProjectsService
    
    +Return all projects with authors, number of commits
    
ListCommitsService
    
    +return collection of commits for given project id

DeleteCommitsService (many)

    +Receive array of commit ids, and soft delete them

### Binding providers

+Ropository bindings + tests

+Services bindings + tests

### CLI commands

Import repo
    
    +php artisan repo:import <repoAlias> (i.e. "author/name")
    +calls list service and return data in different format
    +* maybe add formatter injection

List commits
    
    +php artisan repo:show <repoAlias> (i.e. "author/name")
    +calls ListCommitsService
    +* maybe add formatter injection

### Rest API

GET /api/v1/repo
    
    +return all repos (resource collection, paginated)

GET /api/v1/repo/{project}
    
    +return all commits for repo (res collection, paginated)

POST /api/v1/repo/
    
    +validate author and repo and format of string (custom http rule => project rule)
    +validate only one "/"
    +receive author/repo name, launche import service
    +If exception is thrown
    +Return OK to client (task queed or task done status !)
    +201 created

DELETE /api/v1/repo/{project}
    
    +validate array not empty
    +send list of ids for deletion in array
    +! check if appropriate to send array in DEL request
    +*simplify - doens validate existence
    +return 200 OK and list of deleted id's

### Frontend work

##### Components
AddProject
    
    +after receiving OK - trigger projects list refresh xhr
    +show validation erros if any

ListProjects paginated
    
    +in author/project format, number of commits and open button

ListCommits paginated, option to delete
    
    +author/project at the top
    +list of commits
    +checkboxes for deletion, ALL checkbox (?)
    +if any checkbox active - DELETE button is active 
