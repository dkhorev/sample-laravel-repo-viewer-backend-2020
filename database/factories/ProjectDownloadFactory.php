<?php

/**
 * @var Factory $factory
 */

use App\Models\Project;
use App\Models\ProjectDownload;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(ProjectDownload::class, function (Faker $faker) {
    return [
        'project_id' => factory(Project::class)->create(),
    ];
});
