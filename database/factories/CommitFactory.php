<?php

/**
 * @var Factory $factory
 */

use App\Models\Commit;
use App\Models\Project;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Commit::class, function (Faker $faker, array $options = []) {
    if (array_key_exists('project_id', $options)) {
        $project = $options['project_id'];
    } else {
        $project = factory(Project::class)->create();
    }

    return [
        'sha'        => \Illuminate\Support\Str::random(40),
        'project_id' => $project,
    ];
});
