<?php

/**
 * @var Factory $factory
 */

use App\Models\Project;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Project::class, function (Faker $faker, array $options = []) {
    if (array_key_exists('author_id', $options)) {
        $author = $options['author_id'];
    } else {
        $author = factory(\App\Models\Author::class)->create();
    }

    return [
        'name'      => $faker->unique()->lexify('project?????'),
        'author_id' => $author,
    ];
});
