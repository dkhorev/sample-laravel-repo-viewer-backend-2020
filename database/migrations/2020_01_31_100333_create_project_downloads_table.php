<?php

use App\Constants\ProjectDownloadsStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_downloads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('project_id');
            $table->unsignedSmallInteger('pages_parsed')->default(0);
            $table->enum('status', [
                ProjectDownloadsStatus::STATUS_PROGRESS,
                ProjectDownloadsStatus::STATUS_FINISHED,
            ])->default(ProjectDownloadsStatus::STATUS_PROGRESS);
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_downloads');
    }
}
