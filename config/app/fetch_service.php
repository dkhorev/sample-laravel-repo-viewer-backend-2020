<?php

return [
    'url_template' => [
        'github' => 'https://api.github.com/repos/:author/:project/commits',
    ],

    'per_page' => env('FETCH_PER_PAGE', 20),

    'http_client_options' => [
        'timeout'     => 20,
        'http_errors' => false,
        'headers'     => [
            'accept' => 'application/vnd.github.v3+json',
        ],
    ],
];
